import CategoriesView from "./views/CategoriesView";
import PlacesGridView from "./views/PlacesGridView";
import SearchBarView from "./views/SearchBarView";
import PlacesTrackerController from "./controllers/PlacesTrackerController";
import PlacesTrackerModel from "./models/PlacesTrackerModel";
import PlaceDetailsView from "./views/PlaceDetailsView";

const selectCategoriesViewElement = document.getElementById("categoryList");
const categoriesView = new CategoriesView({ element: selectCategoriesViewElement });

const placesGridViewElement = document.getElementById("places");
const placesGridView = new PlacesGridView({ element: placesGridViewElement });

const placeDetailsViewElement = document.getElementById("details");
const placeDetailsView = new PlaceDetailsView({ element: placeDetailsViewElement });

const searchBarViewElement = document.getElementById("searchBarForm");
const searchBarView = new SearchBarView({ element: searchBarViewElement });

const placesTrackerModel = new PlacesTrackerModel(places);

const placesTrackerController = new PlacesTrackerController({
  categoriesView,
  placesGridView,
  placesTrackerModel,
  searchBarView,
  placeDetailsView,
});

placesTrackerController.start();

window.placesTrackerController = placesTrackerController;
