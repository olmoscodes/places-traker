
/**
 * Indica si el elemento indicado cumple con el selector.
 * @param  {Element} element
 * @param  {string} selector
 * @return {boolean}
 */
function matches(element, selector) {
  return element.matches(selector);
}

/**
 * Ejecuta `handler` cuando ocurre el evento con nombre `eventName`
 * en un elemento contenido en `container` que cumple con el selector `targetSelector`
 * @param  {Element} container
 * @param  {string} eventName
 * @param  {string} targetSelector
 * @param  {Function} handler
 */
function delegateEvent(container, eventName, targetSelector, handler) {
  container.addEventListener(eventName, (event) => {
    let target = event.target;
    while (target !== container && !matches(target, targetSelector)) {
      target = target.parentNode;
    }
    if (target !== container) {
      event.delegateTarget = target;
      handler(event);
    }
  });
}

export default delegateEvent;
