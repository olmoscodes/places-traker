/**
 * Devuelve la representación en forma de elemento HTML a partir del string
 * indicado. string => Element
 * @param  {string} html
 * @return {Element}
 */
function htmlToElement(html) {
  const wrapper = document.createElement('div');
  wrapper.innerHTML = html;
  const element = wrapper.firstElementChild;
  return element;
}

export default htmlToElement;
