import htmlToElement from "../utils/htmlToElement";

var EventEmitter = require("events");

/**
 * Gestiona la vista del lugar más detallado.
 */
class PlaceDetailsView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Muestra los detalles del lugar seleccionado
   * @param  {Object} details
   */
  show(details) {
    this._element.innerHTML = "";
    details.forEach((detail) =>
      this._element.appendChild(renderPlaceDetails(detail))
    );
  }
}

/**
 * Devuelve un elemento HTML que representa los detalles del lugar seleccionado
 * @param  {Object} detail
 * @return {Element}
 */
function renderPlaceDetails(detail) {
  return htmlToElement(`
    <div>
      <h2 class="my-4">Place details</h2>
      <div class="card mt-4">
        <img class="card-img-top img-fluid" src="${detail.photoPlace}" alt="">
        <div class="card-body">
          <div>
            <h3 class="card-title">${detail.namePlace}</h3>
            <span class="text-warning" style="float:right;">${detail.ratingPlace}</span>
          </div>
          <p>
            <strong>${detail.adressPlace}</strong>
          </p>
          <ul>
            <li>${detail.phonePlace}</li>
            <li><span class="text-success">Open</span> - <span class="text-danger">Close</span></li>
            <li><a href="${detail.websitePlace}">Website</a></li>
          </ul>
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          Place Reviews
        </div>
        <div class="card-body">
          <span class="text-warning">${detail.reviewsPlace1Stars}</span>
          ${detail.reviewsPlace1Rating} stars
          <p>${detail.reviewsPlace1Text}</p>
          <small class="text-muted">${detail.reviewsPlace1Author} ${detail.reviewsPlace1Time}</small>
          <hr>
          <span class="text-warning">${detail.reviewsPlace2Stars}</span>
          ${detail.reviewsPlace2Rating} stars
          <p>${detail.reviewsPlace2Text}</p>
          <small class="text-muted">${detail.reviewsPlace2Author} ${detail.reviewsPlace1Time}</small>
          <hr>
          <span class="text-warning">${detail.reviewsPlace3Stars}</span>
          ${detail.reviewsPlace3Rating} stars
          <p>${detail.reviewsPlace3Text}</p>
          <small class="text-muted">${detail.reviewsPlace3Author} ${detail.reviewsPlace1Time}</small>
        </div>
      </div>

    </div>
    `
  );
}

export default PlaceDetailsView;
