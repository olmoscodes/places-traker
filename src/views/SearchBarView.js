var EventEmitter = require("events");

/**
 * Gestiona la vista del la barra de búsqueda de lugares.
 */
class SearchBarView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupSelectSearchBarInput();
  }

  /**
   * Inicia el evento para emitir el input que introduce el usuario en la barra de búsqueda
   * @private
   */
  _setupSelectSearchBarInput() {
    this._element.addEventListener("submit", (e) => {
      e.preventDefault();
      const search = document.getElementById("searchBarInput").value;
      console.log(search)
      this.emit("input", { search });
    });
  }
}

export default SearchBarView;
