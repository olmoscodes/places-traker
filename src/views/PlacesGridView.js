import htmlToElement from "../utils/htmlToElement";
import delegateEvent from "../utils/delegateEvent";

var EventEmitter = require("events");

/**
 * Gestiona la vista del grid de los lugares.
 */
class PlacesGridView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupPlaceIdSelection();
  }

  /**
   * Inicia el evento para seleccionar un lugar de los que se encuentran en el grid de lugares
   * @private
   */
  _setupPlaceIdSelection() {
    delegateEvent(this._element, "click", "[place-id]", (event) => {
      const id = event.delegateTarget.getAttribute("place-id");
      console.log(id);
      this.emit("placeIdSelected", { id });
    });
  }

  /**
   * Muestra los lugares en el grid de lugares
   * @param  {Object} places
   */
  show(places) {
    this._element.innerHTML = "";
    places.forEach((place) =>
      this._element.appendChild(renderPlacesGrid(place))
    );
  }
}

/**
 * Devuelve un elemento HTML que representa los lugares que corresponen a la categoría seleccionada
 * @param  {Object} place
 * @return {Element}
 */
function renderPlacesGrid(place) {
  return htmlToElement(
    ` <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="${place.photos}" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="#" place-id="${place.id}">${place.name}</a>
            </h4>
            <p><strong>${place.vicinity}</strong></p>
          </div>
          <div class="card-footer">
            <small class="text-warning">${place.rating}</small>
          </div>
        </div>
      </div>`
  );
}

export default PlacesGridView;
