import delegateEvent from "../utils/delegateEvent";

var EventEmitter = require("events");

/**
 * Gestiona la vista de las categorias.
 * Emite eventos cuando se selecciona una categoría.
 */
class CategoriesView extends EventEmitter {
  /**
   * @param {Element} options.element
   */
  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupSelectCategory();
  }

  /**
   * Inicia el evento para seleccionar la categoría
   * @private
   */
  _setupSelectCategory() {
    delegateEvent(this._element, "click", "[category-id]", (event) => {
      const id = event.delegateTarget.getAttribute("category-id");
      this.emit("category", { id });
    });
  }
}

export default CategoriesView;
