/**
 * Controla la aplicación que muestra lugares
 */
class PlacesTrackerController {
  /**
   * @param {CategoriesView} options.categoriesView
   * @param {PlacesTrackerModel} options.placesTrackerModel
   * @param {PlacesGridView} options.placesGridView
   * @param {SearchBarView} options.searchBarView
   * @param {PlaceDetailsView} options.placeDetailsView
   */

  constructor({
    categoriesView,
    placesTrackerModel,
    placesGridView,
    searchBarView,
    placeDetailsView,
  }) {
    this._categoriesView = categoriesView;
    this._placesTrackerModel = placesTrackerModel;
    this._placesGridView = placesGridView;
    this._placeDetailsView = placeDetailsView;
    this._searchBarView = searchBarView;
  }

  /**
   * Inicializa el componente para cargar los datos iniciales y empezar a recibir eventos
   */
  start() {
    this._setupCategoriesView();
    this._setupSearchBarView();
    this._setupPlacesGridView();
  }

  /**
   * Configura la vista de las diferentes categorías de lugares
   *  @private
   */
  _setupCategoriesView() {
    this._categoriesView.start();
    this._categoriesView.on("category", (data) =>
      this._onSelectingCategory(data)
    );
  }

  /**
   * Configura la vista del grid de lugares
   *  @private
   */
  _setupPlacesGridView() {
    this._placesGridView.start();
    this._placesGridView.on("placeIdSelected", (data) =>
      this._onSelectingOnePlace(data)
    );
  }

  /**
   * Configura la vista de la barra de búsqueda de lugares
   *  @private
   */
  _setupSearchBarView() {
    this._searchBarView.start();
    this._searchBarView.on("input", (data) =>
      this._onSelectingSearchBarInput((data))
    );
  }

  /**
   * Muestra el grid con la información que nos trae el modelo a partir de la función searchNearByName().
   * @private
   */
     _onSelectingSearchBarInput({ id }) {
      const placesArray = [];
      this._placesTrackerModel
        .searchNearByName(id)
        .then((places) => {
          for (let i = 0; i <= 6; i++) {
            let place = places[i];
            console.log(places);
            if (place.rating == 5) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9733 &#9733 &#9733",
              });
            } else if (place.rating > 4) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                href: null,
                rating: "&#9733 &#9733 &#9733 &#9733 &#9734",
              });
            } else if (place.rating > 3) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9733 &#9734 &#9734",
              });
            } else if (place.rating > 2) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9734 &#9734 &#9734",
              });
            } else if (place.rating > 1) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9734 &#9734 &#9734 &#9734",
              });
            } else if (place.rating > 0) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl({ maxHeight: 30 }),
                rating: "&#9734 &#9734 &#9734 &#9734 &#9734",
              });
            }
          }
          this._placesGridView.show(placesArray);
        })
        .catch((error) => {
          console.log(error);
          return alert("No se han encontrado resultados");
        });
    }
  

  //He encontrado un problema al intentar obtener el booleano de open_now, para
  //poner si está abierto o no el sitio.

  //He visto que "open_now" y "utc_offset" están obsoletos en la documentación de Places Library.

  /**
   * Muestra la sección de Place details con la información del modelo a partir de la función placeDetails().
   * @private
   */
  _onSelectingOnePlace({ id }) {
    const detailsPlaceArray = [];
    this._placesTrackerModel
      .placeDetails(id)
      .then((details) => {
        console.log(details);
        // Estrellas para los comentarios
        if (details.reviews[0].rating == 5) {
          var stars1 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[0].rating >= 4) {
          var stars1 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[0].rating >= 3) {
          var stars1 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 2) {
          var stars1 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 1) {
          var stars1 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 0) {
          var stars1 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.reviews[1].rating == 5) {
          var stars2 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[1].rating >= 4) {
          var stars2 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[1].rating >= 3) {
          var stars2 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 2) {
          var stars2 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 1) {
          var stars2 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 0) {
          var stars2 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.reviews[2].rating == 5) {
          var stars3 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[2].rating >= 4) {
          var stars3 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[2].rating >= 3) {
          var stars3 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 2) {
          var stars3 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 1) {
          var stars3 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 0) {
          var stars3 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.rating == 5) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9733 &#9733",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 4) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9733 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 3) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 2) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 1) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9734 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 0) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9734 &#9734 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        }
        console.log(detailsPlaceArray);
        this._placeDetailsView.show(detailsPlaceArray);
      })
      .catch((error) => {
        console.log(error);
        return alert("No se han encontrado resultados");
      });
  }


  /**
   * Muestra el grid con la información que nos trae el modelo a partir de la función searchNearByType().
   * @private
   */
  _onSelectingCategory({ id }) {
    const placesArray = [];
    this._placesTrackerModel
      .searchNearByType(id)
      .then((places) => {
        for (let i = 0; i < 6; i++) {
          let place = places[i];
          console.log(places);
          if (place.rating == 5) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9733 &#9733 &#9733",
            });
          } else if (place.rating > 4) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              href: null,
              rating: "&#9733 &#9733 &#9733 &#9733 &#9734",
            });
          } else if (place.rating > 3) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9733 &#9734 &#9734",
            });
          } else if (place.rating > 2) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9734 &#9734 &#9734",
            });
          } else if (place.rating > 1) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9734 &#9734 &#9734 &#9734",
            });
          } else if (place.rating > 0) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl({ maxHeight: 30 }),
              rating: "&#9734 &#9734 &#9734 &#9734 &#9734",
            });
          }
        }
        this._placesGridView.show(placesArray);
      })
      .catch((error) => {
        console.log(error);
        return alert("No se han encontrado resultados");
      });
  }
}

export default PlacesTrackerController;
