class PlacesTrackerModel {
  searchNearByType(category) {
    console.log(category);
    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        location: new google.maps.LatLng(41.411549, 2.194118),
        radius: 50000,
        type: [category],
      };

      service.nearbySearch(request, resolve);
    });
  }

  searchNearByName(search) {
    console.log(search);
    // search = 'maquinista';
    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        location: new google.maps.LatLng(41.411549, 2.194118),
        radius: 50000,
        keyword: search,
      };
      service.nearbySearch(request, resolve);
    });
  }

  placeDetails(place_id) {
    console.log(place_id);

    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        placeId: place_id,
        fields: [
          "photo",
          "name",
          "formatted_address",
          "formatted_phone_number",
          "rating",
          "website",
          "reviews",
          "type",
        ],
      };

      service.getDetails(request, resolve);
    });
  }
}

export default PlacesTrackerModel;
