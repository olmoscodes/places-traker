/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/***/ ((module) => {

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;
module.exports.once = once;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}

function once(emitter, name) {
  return new Promise(function (resolve, reject) {
    function errorListener(err) {
      emitter.removeListener(name, resolver);
      reject(err);
    }

    function resolver() {
      if (typeof emitter.removeListener === 'function') {
        emitter.removeListener('error', errorListener);
      }
      resolve([].slice.call(arguments));
    };

    eventTargetAgnosticAddListener(emitter, name, resolver, { once: true });
    if (name !== 'error') {
      addErrorHandlerIfEventEmitter(emitter, errorListener, { once: true });
    }
  });
}

function addErrorHandlerIfEventEmitter(emitter, handler, flags) {
  if (typeof emitter.on === 'function') {
    eventTargetAgnosticAddListener(emitter, 'error', handler, flags);
  }
}

function eventTargetAgnosticAddListener(emitter, name, listener, flags) {
  if (typeof emitter.on === 'function') {
    if (flags.once) {
      emitter.once(name, listener);
    } else {
      emitter.on(name, listener);
    }
  } else if (typeof emitter.addEventListener === 'function') {
    // EventTarget does not have `error` event semantics like Node
    // EventEmitters, we do not listen for `error` events here.
    emitter.addEventListener(name, function wrapListener(arg) {
      // IE does not have builtin `{ once: true }` support so we
      // have to do it manually.
      if (flags.once) {
        emitter.removeEventListener(name, wrapListener);
      }
      listener(arg);
    });
  } else {
    throw new TypeError('The "emitter" argument must be of type EventEmitter. Received type ' + typeof emitter);
  }
}


/***/ }),

/***/ "./src/controllers/PlacesTrackerController.js":
/*!****************************************************!*\
  !*** ./src/controllers/PlacesTrackerController.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/**
 * Controla la aplicación que muestra lugares
 */
class PlacesTrackerController {
  /**
   * @param {CategoriesView} options.categoriesView
   * @param {PlacesTrackerModel} options.placesTrackerModel
   * @param {PlacesGridView} options.placesGridView
   * @param {SearchBarView} options.searchBarView
   * @param {PlaceDetailsView} options.placeDetailsView
   */

  constructor({
    categoriesView,
    placesTrackerModel,
    placesGridView,
    searchBarView,
    placeDetailsView,
  }) {
    this._categoriesView = categoriesView;
    this._placesTrackerModel = placesTrackerModel;
    this._placesGridView = placesGridView;
    this._placeDetailsView = placeDetailsView;
    this._searchBarView = searchBarView;
  }

  /**
   * Inicializa el componente para cargar los datos iniciales y empezar a recibir eventos
   */
  start() {
    this._setupCategoriesView();
    this._setupSearchBarView();
    this._setupPlacesGridView();
  }

  /**
   * Configura la vista de las diferentes categorías de lugares
   *  @private
   */
  _setupCategoriesView() {
    this._categoriesView.start();
    this._categoriesView.on("category", (data) =>
      this._onSelectingCategory(data)
    );
  }

  /**
   * Configura la vista del grid de lugares
   *  @private
   */
  _setupPlacesGridView() {
    this._placesGridView.start();
    this._placesGridView.on("placeIdSelected", (data) =>
      this._onSelectingOnePlace(data)
    );
  }

  /**
   * Configura la vista de la barra de búsqueda de lugares
   *  @private
   */
  _setupSearchBarView() {
    this._searchBarView.start();
    this._searchBarView.on("input", (data) =>
      this._onSelectingSearchBarInput((data))
    );
  }

  /**
   * Muestra el grid con la información que nos trae el modelo a partir de la función searchNearByName().
   * @private
   */
     _onSelectingSearchBarInput({ id }) {
      const placesArray = [];
      this._placesTrackerModel
        .searchNearByName(id)
        .then((places) => {
          for (let i = 0; i <= 6; i++) {
            let place = places[i];
            console.log(places);
            if (place.rating == 5) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9733 &#9733 &#9733",
              });
            } else if (place.rating > 4) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                href: null,
                rating: "&#9733 &#9733 &#9733 &#9733 &#9734",
              });
            } else if (place.rating > 3) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9733 &#9734 &#9734",
              });
            } else if (place.rating > 2) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9733 &#9734 &#9734 &#9734",
              });
            } else if (place.rating > 1) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl(),
                rating: "&#9733 &#9734 &#9734 &#9734 &#9734",
              });
            } else if (place.rating > 0) {
              placesArray.push({
                id: place.place_id,
                name: place.name,
                vicinity: place.vicinity,
                photos: place.photos[0].getUrl({ maxHeight: 30 }),
                rating: "&#9734 &#9734 &#9734 &#9734 &#9734",
              });
            }
          }
          this._placesGridView.show(placesArray);
        })
        .catch((error) => {
          console.log(error);
          return alert("No se han encontrado resultados");
        });
    }
  

  //He encontrado un problema al intentar obtener el booleano de open_now, para
  //poner si está abierto o no el sitio.

  //He visto que "open_now" y "utc_offset" están obsoletos en la documentación de Places Library.

  /**
   * Muestra la sección de Place details con la información del modelo a partir de la función placeDetails().
   * @private
   */
  _onSelectingOnePlace({ id }) {
    const detailsPlaceArray = [];
    this._placesTrackerModel
      .placeDetails(id)
      .then((details) => {
        console.log(details);
        // Estrellas para los comentarios
        if (details.reviews[0].rating == 5) {
          var stars1 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[0].rating >= 4) {
          var stars1 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[0].rating >= 3) {
          var stars1 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 2) {
          var stars1 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 1) {
          var stars1 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[0].rating >= 0) {
          var stars1 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.reviews[1].rating == 5) {
          var stars2 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[1].rating >= 4) {
          var stars2 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[1].rating >= 3) {
          var stars2 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 2) {
          var stars2 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 1) {
          var stars2 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[1].rating >= 0) {
          var stars2 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.reviews[2].rating == 5) {
          var stars3 = "&#9733 &#9733 &#9733 &#9733 &#9733";
        } else if (details.reviews[2].rating >= 4) {
          var stars3 = "&#9733 &#9733 &#9733 &#9733 &#9734";
        } else if (details.reviews[2].rating >= 3) {
          var stars3 = "&#9733 &#9733 &#9733 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 2) {
          var stars3 = "&#9733 &#9733 &#9734 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 1) {
          var stars3 = "&#9733 &#9734 &#9734 &#9734 &#9734";
        } else if (details.reviews[2].rating >= 0) {
          var stars3 = "&#9734 &#9734 &#9734 &#9734 &#9734";
        }

        if (details.rating == 5) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9733 &#9733",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 4) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9733 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 3) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9733 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 2) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9733 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 1) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9733 &#9734 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        } else if (details.rating > 0) {
          detailsPlaceArray.push({
            photoPlace: details.photos[0].getUrl(),
            namePlace: details.name,
            adressPlace: details.formatted_address,
            phonePlace: details.formatted_phone_number,
            ratingPlace: "&#9734 &#9734 &#9734 &#9734 &#9734",
            websitePlace: details.website,

            //Review 1
            reviewsPlace1Author: details.reviews[0].author_name,
            reviewsPlace1Text: details.reviews[0].text,
            reviewsPlace1Time: details.reviews[0].relative_time_description,
            reviewsPlace1Rating: details.reviews[0].rating,
            reviewsPlace1Stars: stars1,

            //Review 2
            reviewsPlace2Author: details.reviews[1].author_name,
            reviewsPlace2Text: details.reviews[1].text,
            reviewsPlace2Time: details.reviews[1].relative_time_description,
            reviewsPlace2Rating: details.reviews[1].rating,
            reviewsPlace2Stars: stars2,

            //Review 2
            reviewsPlace3Author: details.reviews[2].author_name,
            reviewsPlace3Text: details.reviews[2].text,
            reviewsPlace3Time: details.reviews[2].relative_time_description,
            reviewsPlace3Rating: details.reviews[2].rating,
            reviewsPlace3Stars: stars3,
          });
        }
        console.log(detailsPlaceArray);
        this._placeDetailsView.show(detailsPlaceArray);
      })
      .catch((error) => {
        console.log(error);
        return alert("No se han encontrado resultados");
      });
  }


  /**
   * Muestra el grid con la información que nos trae el modelo a partir de la función searchNearByType().
   * @private
   */
  _onSelectingCategory({ id }) {
    const placesArray = [];
    this._placesTrackerModel
      .searchNearByType(id)
      .then((places) => {
        for (let i = 0; i < 6; i++) {
          let place = places[i];
          console.log(places);
          if (place.rating == 5) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9733 &#9733 &#9733",
            });
          } else if (place.rating > 4) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              href: null,
              rating: "&#9733 &#9733 &#9733 &#9733 &#9734",
            });
          } else if (place.rating > 3) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9733 &#9734 &#9734",
            });
          } else if (place.rating > 2) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9733 &#9734 &#9734 &#9734",
            });
          } else if (place.rating > 1) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl(),
              rating: "&#9733 &#9734 &#9734 &#9734 &#9734",
            });
          } else if (place.rating > 0) {
            placesArray.push({
              id: place.place_id,
              name: place.name,
              vicinity: place.vicinity,
              photos: place.photos[0].getUrl({ maxHeight: 30 }),
              rating: "&#9734 &#9734 &#9734 &#9734 &#9734",
            });
          }
        }
        this._placesGridView.show(placesArray);
      })
      .catch((error) => {
        console.log(error);
        return alert("No se han encontrado resultados");
      });
  }
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PlacesTrackerController);


/***/ }),

/***/ "./src/models/PlacesTrackerModel.js":
/*!******************************************!*\
  !*** ./src/models/PlacesTrackerModel.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
class PlacesTrackerModel {
  searchNearByType(category) {
    console.log(category);
    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        location: new google.maps.LatLng(41.411549, 2.194118),
        radius: 50000,
        type: [category],
      };

      service.nearbySearch(request, resolve);
    });
  }

  searchNearByName(search) {
    console.log(search);
    // search = 'maquinista';
    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        location: new google.maps.LatLng(41.411549, 2.194118),
        radius: 50000,
        keyword: search,
      };
      service.nearbySearch(request, resolve);
    });
  }

  placeDetails(place_id) {
    console.log(place_id);

    return new Promise((resolve, reject) => {
      const service = new google.maps.places.PlacesService(
        document.createElement("div")
      );
      let request = {
        placeId: place_id,
        fields: [
          "photo",
          "name",
          "formatted_address",
          "formatted_phone_number",
          "rating",
          "website",
          "reviews",
          "type",
        ],
      };

      service.getDetails(request, resolve);
    });
  }
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PlacesTrackerModel);


/***/ }),

/***/ "./src/utils/delegateEvent.js":
/*!************************************!*\
  !*** ./src/utils/delegateEvent.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });

/**
 * Indica si el elemento indicado cumple con el selector.
 * @param  {Element} element
 * @param  {string} selector
 * @return {boolean}
 */
function matches(element, selector) {
  return element.matches(selector);
}

/**
 * Ejecuta `handler` cuando ocurre el evento con nombre `eventName`
 * en un elemento contenido en `container` que cumple con el selector `targetSelector`
 * @param  {Element} container
 * @param  {string} eventName
 * @param  {string} targetSelector
 * @param  {Function} handler
 */
function delegateEvent(container, eventName, targetSelector, handler) {
  container.addEventListener(eventName, (event) => {
    let target = event.target;
    while (target !== container && !matches(target, targetSelector)) {
      target = target.parentNode;
    }
    if (target !== container) {
      event.delegateTarget = target;
      handler(event);
    }
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (delegateEvent);


/***/ }),

/***/ "./src/utils/htmlToElement.js":
/*!************************************!*\
  !*** ./src/utils/htmlToElement.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/**
 * Devuelve la representación en forma de elemento HTML a partir del string
 * indicado. string => Element
 * @param  {string} html
 * @return {Element}
 */
function htmlToElement(html) {
  const wrapper = document.createElement('div');
  wrapper.innerHTML = html;
  const element = wrapper.firstElementChild;
  return element;
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (htmlToElement);


/***/ }),

/***/ "./src/views/CategoriesView.js":
/*!*************************************!*\
  !*** ./src/views/CategoriesView.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _utils_delegateEvent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/delegateEvent */ "./src/utils/delegateEvent.js");


var EventEmitter = __webpack_require__(/*! events */ "./node_modules/events/events.js");

/**
 * Gestiona la vista de las categorias.
 * Emite eventos cuando se selecciona una categoría.
 */
class CategoriesView extends EventEmitter {
  /**
   * @param {Element} options.element
   */
  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupSelectCategory();
  }

  /**
   * Inicia el evento para seleccionar la categoría
   * @private
   */
  _setupSelectCategory() {
    (0,_utils_delegateEvent__WEBPACK_IMPORTED_MODULE_0__.default)(this._element, "click", "[category-id]", (event) => {
      const id = event.delegateTarget.getAttribute("category-id");
      this.emit("category", { id });
    });
  }
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoriesView);


/***/ }),

/***/ "./src/views/PlaceDetailsView.js":
/*!***************************************!*\
  !*** ./src/views/PlaceDetailsView.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _utils_htmlToElement__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/htmlToElement */ "./src/utils/htmlToElement.js");


var EventEmitter = __webpack_require__(/*! events */ "./node_modules/events/events.js");

/**
 * Gestiona la vista del lugar más detallado.
 */
class PlaceDetailsView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Muestra los detalles del lugar seleccionado
   * @param  {Object} details
   */
  show(details) {
    this._element.innerHTML = "";
    details.forEach((detail) =>
      this._element.appendChild(renderPlaceDetails(detail))
    );
  }
}

/**
 * Devuelve un elemento HTML que representa los detalles del lugar seleccionado
 * @param  {Object} detail
 * @return {Element}
 */
function renderPlaceDetails(detail) {
  return (0,_utils_htmlToElement__WEBPACK_IMPORTED_MODULE_0__.default)(`
    <div>
      <h2 class="my-4">Place details</h2>
      <div class="card mt-4">
        <img class="card-img-top img-fluid" src="${detail.photoPlace}" alt="">
        <div class="card-body">
          <div>
            <h3 class="card-title">${detail.namePlace}</h3>
            <span class="text-warning" style="float:right;">${detail.ratingPlace}</span>
          </div>
          <p>
            <strong>${detail.adressPlace}</strong>
          </p>
          <ul>
            <li>${detail.phonePlace}</li>
            <li><span class="text-success">Open</span> - <span class="text-danger">Close</span></li>
            <li><a href="${detail.websitePlace}">Website</a></li>
          </ul>
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          Place Reviews
        </div>
        <div class="card-body">
          <span class="text-warning">${detail.reviewsPlace1Stars}</span>
          ${detail.reviewsPlace1Rating} stars
          <p>${detail.reviewsPlace1Text}</p>
          <small class="text-muted">${detail.reviewsPlace1Author} ${detail.reviewsPlace1Time}</small>
          <hr>
          <span class="text-warning">${detail.reviewsPlace2Stars}</span>
          ${detail.reviewsPlace2Rating} stars
          <p>${detail.reviewsPlace2Text}</p>
          <small class="text-muted">${detail.reviewsPlace2Author} ${detail.reviewsPlace1Time}</small>
          <hr>
          <span class="text-warning">${detail.reviewsPlace3Stars}</span>
          ${detail.reviewsPlace3Rating} stars
          <p>${detail.reviewsPlace3Text}</p>
          <small class="text-muted">${detail.reviewsPlace3Author} ${detail.reviewsPlace1Time}</small>
        </div>
      </div>

    </div>
    `
  );
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PlaceDetailsView);


/***/ }),

/***/ "./src/views/PlacesGridView.js":
/*!*************************************!*\
  !*** ./src/views/PlacesGridView.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _utils_htmlToElement__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/htmlToElement */ "./src/utils/htmlToElement.js");
/* harmony import */ var _utils_delegateEvent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/delegateEvent */ "./src/utils/delegateEvent.js");



var EventEmitter = __webpack_require__(/*! events */ "./node_modules/events/events.js");

/**
 * Gestiona la vista del grid de los lugares.
 */
class PlacesGridView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupPlaceIdSelection();
  }

  /**
   * Inicia el evento para seleccionar un lugar de los que se encuentran en el grid de lugares
   * @private
   */
  _setupPlaceIdSelection() {
    (0,_utils_delegateEvent__WEBPACK_IMPORTED_MODULE_1__.default)(this._element, "click", "[place-id]", (event) => {
      const id = event.delegateTarget.getAttribute("place-id");
      console.log(id);
      this.emit("placeIdSelected", { id });
    });
  }

  /**
   * Muestra los lugares en el grid de lugares
   * @param  {Object} places
   */
  show(places) {
    this._element.innerHTML = "";
    places.forEach((place) =>
      this._element.appendChild(renderPlacesGrid(place))
    );
  }
}

/**
 * Devuelve un elemento HTML que representa los lugares que corresponen a la categoría seleccionada
 * @param  {Object} place
 * @return {Element}
 */
function renderPlacesGrid(place) {
  return (0,_utils_htmlToElement__WEBPACK_IMPORTED_MODULE_0__.default)(
    ` <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="${place.photos}" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="#" place-id="${place.id}">${place.name}</a>
            </h4>
            <p><strong>${place.vicinity}</strong></p>
          </div>
          <div class="card-footer">
            <small class="text-warning">${place.rating}</small>
          </div>
        </div>
      </div>`
  );
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PlacesGridView);


/***/ }),

/***/ "./src/views/SearchBarView.js":
/*!************************************!*\
  !*** ./src/views/SearchBarView.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var EventEmitter = __webpack_require__(/*! events */ "./node_modules/events/events.js");

/**
 * Gestiona la vista del la barra de búsqueda de lugares.
 */
class SearchBarView extends EventEmitter {
  /**
   * @param {Element} options.element
   */

  constructor({ element }) {
    super();
    this._element = element;
  }

  /**
   * Inicia el componente
   */
  start() {
    this._setupSelectSearchBarInput();
  }

  /**
   * Inicia el evento para emitir el input que introduce el usuario en la barra de búsqueda
   * @private
   */
  _setupSelectSearchBarInput() {
    this._element.addEventListener("submit", (e) => {
      e.preventDefault();
      const search = document.getElementById("searchBarInput").value;
      console.log(search)
      this.emit("input", { search });
    });
  }
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SearchBarView);


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_CategoriesView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./views/CategoriesView */ "./src/views/CategoriesView.js");
/* harmony import */ var _views_PlacesGridView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./views/PlacesGridView */ "./src/views/PlacesGridView.js");
/* harmony import */ var _views_SearchBarView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./views/SearchBarView */ "./src/views/SearchBarView.js");
/* harmony import */ var _controllers_PlacesTrackerController__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./controllers/PlacesTrackerController */ "./src/controllers/PlacesTrackerController.js");
/* harmony import */ var _models_PlacesTrackerModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./models/PlacesTrackerModel */ "./src/models/PlacesTrackerModel.js");
/* harmony import */ var _views_PlaceDetailsView__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./views/PlaceDetailsView */ "./src/views/PlaceDetailsView.js");







const selectCategoriesViewElement = document.getElementById("categoryList");
const categoriesView = new _views_CategoriesView__WEBPACK_IMPORTED_MODULE_0__.default({ element: selectCategoriesViewElement });

const placesGridViewElement = document.getElementById("places");
const placesGridView = new _views_PlacesGridView__WEBPACK_IMPORTED_MODULE_1__.default({ element: placesGridViewElement });

const placeDetailsViewElement = document.getElementById("details");
const placeDetailsView = new _views_PlaceDetailsView__WEBPACK_IMPORTED_MODULE_5__.default({ element: placeDetailsViewElement });

const searchBarViewElement = document.getElementById("searchBarForm");
const searchBarView = new _views_SearchBarView__WEBPACK_IMPORTED_MODULE_2__.default({ element: searchBarViewElement });

const placesTrackerModel = new _models_PlacesTrackerModel__WEBPACK_IMPORTED_MODULE_4__.default(places);

const placesTrackerController = new _controllers_PlacesTrackerController__WEBPACK_IMPORTED_MODULE_3__.default({
  categoriesView,
  placesGridView,
  placesTrackerModel,
  searchBarView,
  placeDetailsView,
});

placesTrackerController.start();

window.placesTrackerController = placesTrackerController;

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvLi9ub2RlX21vZHVsZXMvZXZlbnRzL2V2ZW50cy5qcyIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvLi9zcmMvY29udHJvbGxlcnMvUGxhY2VzVHJhY2tlckNvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vcGxhY2UtZmluZGVyLy4vc3JjL21vZGVscy9QbGFjZXNUcmFja2VyTW9kZWwuanMiLCJ3ZWJwYWNrOi8vcGxhY2UtZmluZGVyLy4vc3JjL3V0aWxzL2RlbGVnYXRlRXZlbnQuanMiLCJ3ZWJwYWNrOi8vcGxhY2UtZmluZGVyLy4vc3JjL3V0aWxzL2h0bWxUb0VsZW1lbnQuanMiLCJ3ZWJwYWNrOi8vcGxhY2UtZmluZGVyLy4vc3JjL3ZpZXdzL0NhdGVnb3JpZXNWaWV3LmpzIiwid2VicGFjazovL3BsYWNlLWZpbmRlci8uL3NyYy92aWV3cy9QbGFjZURldGFpbHNWaWV3LmpzIiwid2VicGFjazovL3BsYWNlLWZpbmRlci8uL3NyYy92aWV3cy9QbGFjZXNHcmlkVmlldy5qcyIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvLi9zcmMvdmlld3MvU2VhcmNoQmFyVmlldy5qcyIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vcGxhY2UtZmluZGVyL3dlYnBhY2svcnVudGltZS9kZWZpbmUgcHJvcGVydHkgZ2V0dGVycyIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly9wbGFjZS1maW5kZXIvLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRWE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1COztBQUVuQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixzQkFBc0I7QUFDdkM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsbUJBQW1CLFNBQVM7QUFDNUI7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQSxpQ0FBaUMsUUFBUTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGlCQUFpQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxRQUFRLHlCQUF5QjtBQUNqQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixnQkFBZ0I7QUFDakM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw2REFBNkQsYUFBYTtBQUMxRTtBQUNBLDZEQUE2RCxhQUFhO0FBQzFFO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsYUFBYTtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQ2hmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxlQUFlO0FBQzVCLGFBQWEsbUJBQW1CO0FBQ2hDLGFBQWEsZUFBZTtBQUM1QixhQUFhLGNBQWM7QUFDM0IsYUFBYSxpQkFBaUI7QUFDOUI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDLEtBQUs7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsUUFBUTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRCxnQkFBZ0I7QUFDaEU7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7O0FBR0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixLQUFLO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixLQUFLO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLE9BQU87QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsZ0JBQWdCO0FBQzlEO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQSxpRUFBZSx1QkFBdUIsRUFBQzs7Ozs7Ozs7Ozs7Ozs7O0FDN2N2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUEsaUVBQWUsa0JBQWtCLEVBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxRGxDO0FBQ0E7QUFDQSxZQUFZLFFBQVE7QUFDcEIsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLFFBQVE7QUFDcEIsWUFBWSxPQUFPO0FBQ25CLFlBQVksT0FBTztBQUNuQixZQUFZLFNBQVM7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQSxpRUFBZSxhQUFhLEVBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ2hDN0I7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpRUFBZSxhQUFhLEVBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUNic0I7O0FBRW5ELG1CQUFtQixtQkFBTyxDQUFDLCtDQUFROztBQUVuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckI7QUFDQSxlQUFlLFVBQVU7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSw2REFBYTtBQUNqQjtBQUNBLDZCQUE2QixLQUFLO0FBQ2xDLEtBQUs7QUFDTDtBQUNBOztBQUVBLGlFQUFlLGNBQWMsRUFBQzs7Ozs7Ozs7Ozs7Ozs7OztBQ3BDcUI7O0FBRW5ELG1CQUFtQixtQkFBTyxDQUFDLCtDQUFROztBQUVuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCOztBQUVBLGVBQWUsVUFBVTtBQUN6QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGNBQWMsT0FBTztBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWTtBQUNaO0FBQ0E7QUFDQSxTQUFTLDZEQUFhO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBLG1EQUFtRCxrQkFBa0I7QUFDckU7QUFDQTtBQUNBLHFDQUFxQyxpQkFBaUI7QUFDdEQsMERBQTBELElBQUksbUJBQW1CO0FBQ2pGO0FBQ0E7QUFDQSxzQkFBc0IsbUJBQW1CO0FBQ3pDO0FBQ0E7QUFDQSxrQkFBa0Isa0JBQWtCO0FBQ3BDO0FBQ0EsMkJBQTJCLG9CQUFvQjtBQUMvQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QywwQkFBMEI7QUFDakUsWUFBWSwyQkFBMkI7QUFDdkMsZUFBZSx5QkFBeUI7QUFDeEMsc0NBQXNDLDJCQUEyQixHQUFHLHlCQUF5QjtBQUM3RjtBQUNBLHVDQUF1QywwQkFBMEI7QUFDakUsWUFBWSwyQkFBMkI7QUFDdkMsZUFBZSx5QkFBeUI7QUFDeEMsc0NBQXNDLDJCQUEyQixHQUFHLHlCQUF5QjtBQUM3RjtBQUNBLHVDQUF1QywwQkFBMEI7QUFDakUsWUFBWSwyQkFBMkI7QUFDdkMsZUFBZSx5QkFBeUI7QUFDeEMsc0NBQXNDLDJCQUEyQixHQUFHLHlCQUF5QjtBQUM3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlFQUFlLGdCQUFnQixFQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ25GbUI7QUFDQTs7QUFFbkQsbUJBQW1CLG1CQUFPLENBQUMsK0NBQVE7O0FBRW5DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckI7O0FBRUEsZUFBZSxVQUFVO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksNkRBQWE7QUFDakI7QUFDQTtBQUNBLG9DQUFvQyxLQUFLO0FBQ3pDLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0EsY0FBYyxPQUFPO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBLFNBQVMsNkRBQWE7QUFDdEI7QUFDQTtBQUNBLHVEQUF1RCxhQUFhO0FBQ3BFO0FBQ0E7QUFDQSxzQ0FBc0MsU0FBUyxJQUFJLFdBQVc7QUFDOUQ7QUFDQSx5QkFBeUIsZUFBZTtBQUN4QztBQUNBO0FBQ0EsMENBQTBDLGFBQWE7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpRUFBZSxjQUFjLEVBQUM7Ozs7Ozs7Ozs7Ozs7OztBQ3pFOUIsbUJBQW1CLG1CQUFPLENBQUMsK0NBQVE7O0FBRW5DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckI7O0FBRUEsZUFBZSxVQUFVO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLFNBQVM7QUFDbkMsS0FBSztBQUNMO0FBQ0E7O0FBRUEsaUVBQWUsYUFBYSxFQUFDOzs7Ozs7O1VDcEM3QjtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7OztXQ3RCQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHdDQUF3Qyx5Q0FBeUM7V0FDakY7V0FDQTtXQUNBLEU7Ozs7O1dDUEEsd0Y7Ozs7O1dDQUE7V0FDQTtXQUNBO1dBQ0Esc0RBQXNELGtCQUFrQjtXQUN4RTtXQUNBLCtDQUErQyxjQUFjO1dBQzdELEU7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTm9EO0FBQ0E7QUFDRjtBQUMwQjtBQUNmO0FBQ0w7O0FBRXhEO0FBQ0EsMkJBQTJCLDBEQUFjLEVBQUUsdUNBQXVDOztBQUVsRjtBQUNBLDJCQUEyQiwwREFBYyxFQUFFLGlDQUFpQzs7QUFFNUU7QUFDQSw2QkFBNkIsNERBQWdCLEVBQUUsbUNBQW1DOztBQUVsRjtBQUNBLDBCQUEwQix5REFBYSxFQUFFLGdDQUFnQzs7QUFFekUsK0JBQStCLCtEQUFrQjs7QUFFakQsb0NBQW9DLHlFQUF1QjtBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFIgPSB0eXBlb2YgUmVmbGVjdCA9PT0gJ29iamVjdCcgPyBSZWZsZWN0IDogbnVsbFxudmFyIFJlZmxlY3RBcHBseSA9IFIgJiYgdHlwZW9mIFIuYXBwbHkgPT09ICdmdW5jdGlvbidcbiAgPyBSLmFwcGx5XG4gIDogZnVuY3Rpb24gUmVmbGVjdEFwcGx5KHRhcmdldCwgcmVjZWl2ZXIsIGFyZ3MpIHtcbiAgICByZXR1cm4gRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwodGFyZ2V0LCByZWNlaXZlciwgYXJncyk7XG4gIH1cblxudmFyIFJlZmxlY3RPd25LZXlzXG5pZiAoUiAmJiB0eXBlb2YgUi5vd25LZXlzID09PSAnZnVuY3Rpb24nKSB7XG4gIFJlZmxlY3RPd25LZXlzID0gUi5vd25LZXlzXG59IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KVxuICAgICAgLmNvbmNhdChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHRhcmdldCkpO1xuICB9O1xufSBlbHNlIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KTtcbiAgfTtcbn1cblxuZnVuY3Rpb24gUHJvY2Vzc0VtaXRXYXJuaW5nKHdhcm5pbmcpIHtcbiAgaWYgKGNvbnNvbGUgJiYgY29uc29sZS53YXJuKSBjb25zb2xlLndhcm4od2FybmluZyk7XG59XG5cbnZhciBOdW1iZXJJc05hTiA9IE51bWJlci5pc05hTiB8fCBmdW5jdGlvbiBOdW1iZXJJc05hTih2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUgIT09IHZhbHVlO1xufVxuXG5mdW5jdGlvbiBFdmVudEVtaXR0ZXIoKSB7XG4gIEV2ZW50RW1pdHRlci5pbml0LmNhbGwodGhpcyk7XG59XG5tb2R1bGUuZXhwb3J0cyA9IEV2ZW50RW1pdHRlcjtcbm1vZHVsZS5leHBvcnRzLm9uY2UgPSBvbmNlO1xuXG4vLyBCYWNrd2FyZHMtY29tcGF0IHdpdGggbm9kZSAwLjEwLnhcbkV2ZW50RW1pdHRlci5FdmVudEVtaXR0ZXIgPSBFdmVudEVtaXR0ZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX2V2ZW50cyA9IHVuZGVmaW5lZDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX2V2ZW50c0NvdW50ID0gMDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX21heExpc3RlbmVycyA9IHVuZGVmaW5lZDtcblxuLy8gQnkgZGVmYXVsdCBFdmVudEVtaXR0ZXJzIHdpbGwgcHJpbnQgYSB3YXJuaW5nIGlmIG1vcmUgdGhhbiAxMCBsaXN0ZW5lcnMgYXJlXG4vLyBhZGRlZCB0byBpdC4gVGhpcyBpcyBhIHVzZWZ1bCBkZWZhdWx0IHdoaWNoIGhlbHBzIGZpbmRpbmcgbWVtb3J5IGxlYWtzLlxudmFyIGRlZmF1bHRNYXhMaXN0ZW5lcnMgPSAxMDtcblxuZnVuY3Rpb24gY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcikge1xuICBpZiAodHlwZW9mIGxpc3RlbmVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignVGhlIFwibGlzdGVuZXJcIiBhcmd1bWVudCBtdXN0IGJlIG9mIHR5cGUgRnVuY3Rpb24uIFJlY2VpdmVkIHR5cGUgJyArIHR5cGVvZiBsaXN0ZW5lcik7XG4gIH1cbn1cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KEV2ZW50RW1pdHRlciwgJ2RlZmF1bHRNYXhMaXN0ZW5lcnMnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIGRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gIH0sXG4gIHNldDogZnVuY3Rpb24oYXJnKSB7XG4gICAgaWYgKHR5cGVvZiBhcmcgIT09ICdudW1iZXInIHx8IGFyZyA8IDAgfHwgTnVtYmVySXNOYU4oYXJnKSkge1xuICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RoZSB2YWx1ZSBvZiBcImRlZmF1bHRNYXhMaXN0ZW5lcnNcIiBpcyBvdXQgb2YgcmFuZ2UuIEl0IG11c3QgYmUgYSBub24tbmVnYXRpdmUgbnVtYmVyLiBSZWNlaXZlZCAnICsgYXJnICsgJy4nKTtcbiAgICB9XG4gICAgZGVmYXVsdE1heExpc3RlbmVycyA9IGFyZztcbiAgfVxufSk7XG5cbkV2ZW50RW1pdHRlci5pbml0ID0gZnVuY3Rpb24oKSB7XG5cbiAgaWYgKHRoaXMuX2V2ZW50cyA9PT0gdW5kZWZpbmVkIHx8XG4gICAgICB0aGlzLl9ldmVudHMgPT09IE9iamVjdC5nZXRQcm90b3R5cGVPZih0aGlzKS5fZXZlbnRzKSB7XG4gICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gIH1cblxuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSB0aGlzLl9tYXhMaXN0ZW5lcnMgfHwgdW5kZWZpbmVkO1xufTtcblxuLy8gT2J2aW91c2x5IG5vdCBhbGwgRW1pdHRlcnMgc2hvdWxkIGJlIGxpbWl0ZWQgdG8gMTAuIFRoaXMgZnVuY3Rpb24gYWxsb3dzXG4vLyB0aGF0IHRvIGJlIGluY3JlYXNlZC4gU2V0IHRvIHplcm8gZm9yIHVubGltaXRlZC5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuc2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24gc2V0TWF4TGlzdGVuZXJzKG4pIHtcbiAgaWYgKHR5cGVvZiBuICE9PSAnbnVtYmVyJyB8fCBuIDwgMCB8fCBOdW1iZXJJc05hTihuKSkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdUaGUgdmFsdWUgb2YgXCJuXCIgaXMgb3V0IG9mIHJhbmdlLiBJdCBtdXN0IGJlIGEgbm9uLW5lZ2F0aXZlIG51bWJlci4gUmVjZWl2ZWQgJyArIG4gKyAnLicpO1xuICB9XG4gIHRoaXMuX21heExpc3RlbmVycyA9IG47XG4gIHJldHVybiB0aGlzO1xufTtcblxuZnVuY3Rpb24gX2dldE1heExpc3RlbmVycyh0aGF0KSB7XG4gIGlmICh0aGF0Ll9tYXhMaXN0ZW5lcnMgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gRXZlbnRFbWl0dGVyLmRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gIHJldHVybiB0aGF0Ll9tYXhMaXN0ZW5lcnM7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZ2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24gZ2V0TWF4TGlzdGVuZXJzKCkge1xuICByZXR1cm4gX2dldE1heExpc3RlbmVycyh0aGlzKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZW1pdCA9IGZ1bmN0aW9uIGVtaXQodHlwZSkge1xuICB2YXIgYXJncyA9IFtdO1xuICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgYXJncy5wdXNoKGFyZ3VtZW50c1tpXSk7XG4gIHZhciBkb0Vycm9yID0gKHR5cGUgPT09ICdlcnJvcicpO1xuXG4gIHZhciBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gIGlmIChldmVudHMgIT09IHVuZGVmaW5lZClcbiAgICBkb0Vycm9yID0gKGRvRXJyb3IgJiYgZXZlbnRzLmVycm9yID09PSB1bmRlZmluZWQpO1xuICBlbHNlIGlmICghZG9FcnJvcilcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgLy8gSWYgdGhlcmUgaXMgbm8gJ2Vycm9yJyBldmVudCBsaXN0ZW5lciB0aGVuIHRocm93LlxuICBpZiAoZG9FcnJvcikge1xuICAgIHZhciBlcjtcbiAgICBpZiAoYXJncy5sZW5ndGggPiAwKVxuICAgICAgZXIgPSBhcmdzWzBdO1xuICAgIGlmIChlciBpbnN0YW5jZW9mIEVycm9yKSB7XG4gICAgICAvLyBOb3RlOiBUaGUgY29tbWVudHMgb24gdGhlIGB0aHJvd2AgbGluZXMgYXJlIGludGVudGlvbmFsLCB0aGV5IHNob3dcbiAgICAgIC8vIHVwIGluIE5vZGUncyBvdXRwdXQgaWYgdGhpcyByZXN1bHRzIGluIGFuIHVuaGFuZGxlZCBleGNlcHRpb24uXG4gICAgICB0aHJvdyBlcjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgICB9XG4gICAgLy8gQXQgbGVhc3QgZ2l2ZSBzb21lIGtpbmQgb2YgY29udGV4dCB0byB0aGUgdXNlclxuICAgIHZhciBlcnIgPSBuZXcgRXJyb3IoJ1VuaGFuZGxlZCBlcnJvci4nICsgKGVyID8gJyAoJyArIGVyLm1lc3NhZ2UgKyAnKScgOiAnJykpO1xuICAgIGVyci5jb250ZXh0ID0gZXI7XG4gICAgdGhyb3cgZXJyOyAvLyBVbmhhbmRsZWQgJ2Vycm9yJyBldmVudFxuICB9XG5cbiAgdmFyIGhhbmRsZXIgPSBldmVudHNbdHlwZV07XG5cbiAgaWYgKGhhbmRsZXIgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgaWYgKHR5cGVvZiBoYW5kbGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgUmVmbGVjdEFwcGx5KGhhbmRsZXIsIHRoaXMsIGFyZ3MpO1xuICB9IGVsc2Uge1xuICAgIHZhciBsZW4gPSBoYW5kbGVyLmxlbmd0aDtcbiAgICB2YXIgbGlzdGVuZXJzID0gYXJyYXlDbG9uZShoYW5kbGVyLCBsZW4pO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyArK2kpXG4gICAgICBSZWZsZWN0QXBwbHkobGlzdGVuZXJzW2ldLCB0aGlzLCBhcmdzKTtcbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufTtcblxuZnVuY3Rpb24gX2FkZExpc3RlbmVyKHRhcmdldCwgdHlwZSwgbGlzdGVuZXIsIHByZXBlbmQpIHtcbiAgdmFyIG07XG4gIHZhciBldmVudHM7XG4gIHZhciBleGlzdGluZztcblxuICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcblxuICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcbiAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgIHRhcmdldC5fZXZlbnRzQ291bnQgPSAwO1xuICB9IGVsc2Uge1xuICAgIC8vIFRvIGF2b2lkIHJlY3Vyc2lvbiBpbiB0aGUgY2FzZSB0aGF0IHR5cGUgPT09IFwibmV3TGlzdGVuZXJcIiEgQmVmb3JlXG4gICAgLy8gYWRkaW5nIGl0IHRvIHRoZSBsaXN0ZW5lcnMsIGZpcnN0IGVtaXQgXCJuZXdMaXN0ZW5lclwiLlxuICAgIGlmIChldmVudHMubmV3TGlzdGVuZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgdGFyZ2V0LmVtaXQoJ25ld0xpc3RlbmVyJywgdHlwZSxcbiAgICAgICAgICAgICAgICAgIGxpc3RlbmVyLmxpc3RlbmVyID8gbGlzdGVuZXIubGlzdGVuZXIgOiBsaXN0ZW5lcik7XG5cbiAgICAgIC8vIFJlLWFzc2lnbiBgZXZlbnRzYCBiZWNhdXNlIGEgbmV3TGlzdGVuZXIgaGFuZGxlciBjb3VsZCBoYXZlIGNhdXNlZCB0aGVcbiAgICAgIC8vIHRoaXMuX2V2ZW50cyB0byBiZSBhc3NpZ25lZCB0byBhIG5ldyBvYmplY3RcbiAgICAgIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuICAgIH1cbiAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXTtcbiAgfVxuXG4gIGlmIChleGlzdGluZyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgLy8gT3B0aW1pemUgdGhlIGNhc2Ugb2Ygb25lIGxpc3RlbmVyLiBEb24ndCBuZWVkIHRoZSBleHRyYSBhcnJheSBvYmplY3QuXG4gICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV0gPSBsaXN0ZW5lcjtcbiAgICArK3RhcmdldC5fZXZlbnRzQ291bnQ7XG4gIH0gZWxzZSB7XG4gICAgaWYgKHR5cGVvZiBleGlzdGluZyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgLy8gQWRkaW5nIHRoZSBzZWNvbmQgZWxlbWVudCwgbmVlZCB0byBjaGFuZ2UgdG8gYXJyYXkuXG4gICAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXSA9XG4gICAgICAgIHByZXBlbmQgPyBbbGlzdGVuZXIsIGV4aXN0aW5nXSA6IFtleGlzdGluZywgbGlzdGVuZXJdO1xuICAgICAgLy8gSWYgd2UndmUgYWxyZWFkeSBnb3QgYW4gYXJyYXksIGp1c3QgYXBwZW5kLlxuICAgIH0gZWxzZSBpZiAocHJlcGVuZCkge1xuICAgICAgZXhpc3RpbmcudW5zaGlmdChsaXN0ZW5lcik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGV4aXN0aW5nLnB1c2gobGlzdGVuZXIpO1xuICAgIH1cblxuICAgIC8vIENoZWNrIGZvciBsaXN0ZW5lciBsZWFrXG4gICAgbSA9IF9nZXRNYXhMaXN0ZW5lcnModGFyZ2V0KTtcbiAgICBpZiAobSA+IDAgJiYgZXhpc3RpbmcubGVuZ3RoID4gbSAmJiAhZXhpc3Rpbmcud2FybmVkKSB7XG4gICAgICBleGlzdGluZy53YXJuZWQgPSB0cnVlO1xuICAgICAgLy8gTm8gZXJyb3IgY29kZSBmb3IgdGhpcyBzaW5jZSBpdCBpcyBhIFdhcm5pbmdcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1yZXN0cmljdGVkLXN5bnRheFxuICAgICAgdmFyIHcgPSBuZXcgRXJyb3IoJ1Bvc3NpYmxlIEV2ZW50RW1pdHRlciBtZW1vcnkgbGVhayBkZXRlY3RlZC4gJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nLmxlbmd0aCArICcgJyArIFN0cmluZyh0eXBlKSArICcgbGlzdGVuZXJzICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAnYWRkZWQuIFVzZSBlbWl0dGVyLnNldE1heExpc3RlbmVycygpIHRvICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAnaW5jcmVhc2UgbGltaXQnKTtcbiAgICAgIHcubmFtZSA9ICdNYXhMaXN0ZW5lcnNFeGNlZWRlZFdhcm5pbmcnO1xuICAgICAgdy5lbWl0dGVyID0gdGFyZ2V0O1xuICAgICAgdy50eXBlID0gdHlwZTtcbiAgICAgIHcuY291bnQgPSBleGlzdGluZy5sZW5ndGg7XG4gICAgICBQcm9jZXNzRW1pdFdhcm5pbmcodyk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lciA9IGZ1bmN0aW9uIGFkZExpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gIHJldHVybiBfYWRkTGlzdGVuZXIodGhpcywgdHlwZSwgbGlzdGVuZXIsIGZhbHNlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub24gPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnByZXBlbmRMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcHJlcGVuZExpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICByZXR1cm4gX2FkZExpc3RlbmVyKHRoaXMsIHR5cGUsIGxpc3RlbmVyLCB0cnVlKTtcbiAgICB9O1xuXG5mdW5jdGlvbiBvbmNlV3JhcHBlcigpIHtcbiAgaWYgKCF0aGlzLmZpcmVkKSB7XG4gICAgdGhpcy50YXJnZXQucmVtb3ZlTGlzdGVuZXIodGhpcy50eXBlLCB0aGlzLndyYXBGbik7XG4gICAgdGhpcy5maXJlZCA9IHRydWU7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApXG4gICAgICByZXR1cm4gdGhpcy5saXN0ZW5lci5jYWxsKHRoaXMudGFyZ2V0KTtcbiAgICByZXR1cm4gdGhpcy5saXN0ZW5lci5hcHBseSh0aGlzLnRhcmdldCwgYXJndW1lbnRzKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBfb25jZVdyYXAodGFyZ2V0LCB0eXBlLCBsaXN0ZW5lcikge1xuICB2YXIgc3RhdGUgPSB7IGZpcmVkOiBmYWxzZSwgd3JhcEZuOiB1bmRlZmluZWQsIHRhcmdldDogdGFyZ2V0LCB0eXBlOiB0eXBlLCBsaXN0ZW5lcjogbGlzdGVuZXIgfTtcbiAgdmFyIHdyYXBwZWQgPSBvbmNlV3JhcHBlci5iaW5kKHN0YXRlKTtcbiAgd3JhcHBlZC5saXN0ZW5lciA9IGxpc3RlbmVyO1xuICBzdGF0ZS53cmFwRm4gPSB3cmFwcGVkO1xuICByZXR1cm4gd3JhcHBlZDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24gb25jZSh0eXBlLCBsaXN0ZW5lcikge1xuICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcbiAgdGhpcy5vbih0eXBlLCBfb25jZVdyYXAodGhpcywgdHlwZSwgbGlzdGVuZXIpKTtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnByZXBlbmRPbmNlTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHByZXBlbmRPbmNlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuICAgICAgdGhpcy5wcmVwZW5kTGlzdGVuZXIodHlwZSwgX29uY2VXcmFwKHRoaXMsIHR5cGUsIGxpc3RlbmVyKSk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4vLyBFbWl0cyBhICdyZW1vdmVMaXN0ZW5lcicgZXZlbnQgaWYgYW5kIG9ubHkgaWYgdGhlIGxpc3RlbmVyIHdhcyByZW1vdmVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIHZhciBsaXN0LCBldmVudHMsIHBvc2l0aW9uLCBpLCBvcmlnaW5hbExpc3RlbmVyO1xuXG4gICAgICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcblxuICAgICAgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICAgICAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgbGlzdCA9IGV2ZW50c1t0eXBlXTtcbiAgICAgIGlmIChsaXN0ID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICBpZiAobGlzdCA9PT0gbGlzdGVuZXIgfHwgbGlzdC5saXN0ZW5lciA9PT0gbGlzdGVuZXIpIHtcbiAgICAgICAgaWYgKC0tdGhpcy5fZXZlbnRzQ291bnQgPT09IDApXG4gICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgZGVsZXRlIGV2ZW50c1t0eXBlXTtcbiAgICAgICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyKVxuICAgICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmVMaXN0ZW5lcicsIHR5cGUsIGxpc3QubGlzdGVuZXIgfHwgbGlzdGVuZXIpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiBsaXN0ICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHBvc2l0aW9uID0gLTE7XG5cbiAgICAgICAgZm9yIChpID0gbGlzdC5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgIGlmIChsaXN0W2ldID09PSBsaXN0ZW5lciB8fCBsaXN0W2ldLmxpc3RlbmVyID09PSBsaXN0ZW5lcikge1xuICAgICAgICAgICAgb3JpZ2luYWxMaXN0ZW5lciA9IGxpc3RbaV0ubGlzdGVuZXI7XG4gICAgICAgICAgICBwb3NpdGlvbiA9IGk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocG9zaXRpb24gPCAwKVxuICAgICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICAgIGlmIChwb3NpdGlvbiA9PT0gMClcbiAgICAgICAgICBsaXN0LnNoaWZ0KCk7XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHNwbGljZU9uZShsaXN0LCBwb3NpdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobGlzdC5sZW5ndGggPT09IDEpXG4gICAgICAgICAgZXZlbnRzW3R5cGVdID0gbGlzdFswXTtcblxuICAgICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmVMaXN0ZW5lcicsIHR5cGUsIG9yaWdpbmFsTGlzdGVuZXIgfHwgbGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9mZiA9IEV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID1cbiAgICBmdW5jdGlvbiByZW1vdmVBbGxMaXN0ZW5lcnModHlwZSkge1xuICAgICAgdmFyIGxpc3RlbmVycywgZXZlbnRzLCBpO1xuXG4gICAgICBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gICAgICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICAvLyBub3QgbGlzdGVuaW5nIGZvciByZW1vdmVMaXN0ZW5lciwgbm8gbmVlZCB0byBlbWl0XG4gICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgICAgICAgfSBlbHNlIGlmIChldmVudHNbdHlwZV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGlmICgtLXRoaXMuX2V2ZW50c0NvdW50ID09PSAwKVxuICAgICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBkZWxldGUgZXZlbnRzW3R5cGVdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuXG4gICAgICAvLyBlbWl0IHJlbW92ZUxpc3RlbmVyIGZvciBhbGwgbGlzdGVuZXJzIG9uIGFsbCBldmVudHNcbiAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoZXZlbnRzKTtcbiAgICAgICAgdmFyIGtleTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICBrZXkgPSBrZXlzW2ldO1xuICAgICAgICAgIGlmIChrZXkgPT09ICdyZW1vdmVMaXN0ZW5lcicpIGNvbnRpbnVlO1xuICAgICAgICAgIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKGtleSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoJ3JlbW92ZUxpc3RlbmVyJyk7XG4gICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9XG5cbiAgICAgIGxpc3RlbmVycyA9IGV2ZW50c1t0eXBlXTtcblxuICAgICAgaWYgKHR5cGVvZiBsaXN0ZW5lcnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcnMpO1xuICAgICAgfSBlbHNlIGlmIChsaXN0ZW5lcnMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAvLyBMSUZPIG9yZGVyXG4gICAgICAgIGZvciAoaSA9IGxpc3RlbmVycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzW2ldKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG5mdW5jdGlvbiBfbGlzdGVuZXJzKHRhcmdldCwgdHlwZSwgdW53cmFwKSB7XG4gIHZhciBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcblxuICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIFtdO1xuXG4gIHZhciBldmxpc3RlbmVyID0gZXZlbnRzW3R5cGVdO1xuICBpZiAoZXZsaXN0ZW5lciA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBbXTtcblxuICBpZiAodHlwZW9mIGV2bGlzdGVuZXIgPT09ICdmdW5jdGlvbicpXG4gICAgcmV0dXJuIHVud3JhcCA/IFtldmxpc3RlbmVyLmxpc3RlbmVyIHx8IGV2bGlzdGVuZXJdIDogW2V2bGlzdGVuZXJdO1xuXG4gIHJldHVybiB1bndyYXAgP1xuICAgIHVud3JhcExpc3RlbmVycyhldmxpc3RlbmVyKSA6IGFycmF5Q2xvbmUoZXZsaXN0ZW5lciwgZXZsaXN0ZW5lci5sZW5ndGgpO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVycyA9IGZ1bmN0aW9uIGxpc3RlbmVycyh0eXBlKSB7XG4gIHJldHVybiBfbGlzdGVuZXJzKHRoaXMsIHR5cGUsIHRydWUpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yYXdMaXN0ZW5lcnMgPSBmdW5jdGlvbiByYXdMaXN0ZW5lcnModHlwZSkge1xuICByZXR1cm4gX2xpc3RlbmVycyh0aGlzLCB0eXBlLCBmYWxzZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIubGlzdGVuZXJDb3VudCA9IGZ1bmN0aW9uKGVtaXR0ZXIsIHR5cGUpIHtcbiAgaWYgKHR5cGVvZiBlbWl0dGVyLmxpc3RlbmVyQ291bnQgPT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm4gZW1pdHRlci5saXN0ZW5lckNvdW50KHR5cGUpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBsaXN0ZW5lckNvdW50LmNhbGwoZW1pdHRlciwgdHlwZSk7XG4gIH1cbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJDb3VudCA9IGxpc3RlbmVyQ291bnQ7XG5mdW5jdGlvbiBsaXN0ZW5lckNvdW50KHR5cGUpIHtcbiAgdmFyIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcblxuICBpZiAoZXZlbnRzICE9PSB1bmRlZmluZWQpIHtcbiAgICB2YXIgZXZsaXN0ZW5lciA9IGV2ZW50c1t0eXBlXTtcblxuICAgIGlmICh0eXBlb2YgZXZsaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmV0dXJuIDE7XG4gICAgfSBlbHNlIGlmIChldmxpc3RlbmVyICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiBldmxpc3RlbmVyLmxlbmd0aDtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gMDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5ldmVudE5hbWVzID0gZnVuY3Rpb24gZXZlbnROYW1lcygpIHtcbiAgcmV0dXJuIHRoaXMuX2V2ZW50c0NvdW50ID4gMCA/IFJlZmxlY3RPd25LZXlzKHRoaXMuX2V2ZW50cykgOiBbXTtcbn07XG5cbmZ1bmN0aW9uIGFycmF5Q2xvbmUoYXJyLCBuKSB7XG4gIHZhciBjb3B5ID0gbmV3IEFycmF5KG4pO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IG47ICsraSlcbiAgICBjb3B5W2ldID0gYXJyW2ldO1xuICByZXR1cm4gY29weTtcbn1cblxuZnVuY3Rpb24gc3BsaWNlT25lKGxpc3QsIGluZGV4KSB7XG4gIGZvciAoOyBpbmRleCArIDEgPCBsaXN0Lmxlbmd0aDsgaW5kZXgrKylcbiAgICBsaXN0W2luZGV4XSA9IGxpc3RbaW5kZXggKyAxXTtcbiAgbGlzdC5wb3AoKTtcbn1cblxuZnVuY3Rpb24gdW53cmFwTGlzdGVuZXJzKGFycikge1xuICB2YXIgcmV0ID0gbmV3IEFycmF5KGFyci5sZW5ndGgpO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHJldC5sZW5ndGg7ICsraSkge1xuICAgIHJldFtpXSA9IGFycltpXS5saXN0ZW5lciB8fCBhcnJbaV07XG4gIH1cbiAgcmV0dXJuIHJldDtcbn1cblxuZnVuY3Rpb24gb25jZShlbWl0dGVyLCBuYW1lKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgZnVuY3Rpb24gZXJyb3JMaXN0ZW5lcihlcnIpIHtcbiAgICAgIGVtaXR0ZXIucmVtb3ZlTGlzdGVuZXIobmFtZSwgcmVzb2x2ZXIpO1xuICAgICAgcmVqZWN0KGVycik7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVzb2x2ZXIoKSB7XG4gICAgICBpZiAodHlwZW9mIGVtaXR0ZXIucmVtb3ZlTGlzdGVuZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgZW1pdHRlci5yZW1vdmVMaXN0ZW5lcignZXJyb3InLCBlcnJvckxpc3RlbmVyKTtcbiAgICAgIH1cbiAgICAgIHJlc29sdmUoW10uc2xpY2UuY2FsbChhcmd1bWVudHMpKTtcbiAgICB9O1xuXG4gICAgZXZlbnRUYXJnZXRBZ25vc3RpY0FkZExpc3RlbmVyKGVtaXR0ZXIsIG5hbWUsIHJlc29sdmVyLCB7IG9uY2U6IHRydWUgfSk7XG4gICAgaWYgKG5hbWUgIT09ICdlcnJvcicpIHtcbiAgICAgIGFkZEVycm9ySGFuZGxlcklmRXZlbnRFbWl0dGVyKGVtaXR0ZXIsIGVycm9yTGlzdGVuZXIsIHsgb25jZTogdHJ1ZSB9KTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBhZGRFcnJvckhhbmRsZXJJZkV2ZW50RW1pdHRlcihlbWl0dGVyLCBoYW5kbGVyLCBmbGFncykge1xuICBpZiAodHlwZW9mIGVtaXR0ZXIub24gPT09ICdmdW5jdGlvbicpIHtcbiAgICBldmVudFRhcmdldEFnbm9zdGljQWRkTGlzdGVuZXIoZW1pdHRlciwgJ2Vycm9yJywgaGFuZGxlciwgZmxhZ3MpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGV2ZW50VGFyZ2V0QWdub3N0aWNBZGRMaXN0ZW5lcihlbWl0dGVyLCBuYW1lLCBsaXN0ZW5lciwgZmxhZ3MpIHtcbiAgaWYgKHR5cGVvZiBlbWl0dGVyLm9uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgaWYgKGZsYWdzLm9uY2UpIHtcbiAgICAgIGVtaXR0ZXIub25jZShuYW1lLCBsaXN0ZW5lcik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVtaXR0ZXIub24obmFtZSwgbGlzdGVuZXIpO1xuICAgIH1cbiAgfSBlbHNlIGlmICh0eXBlb2YgZW1pdHRlci5hZGRFdmVudExpc3RlbmVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgLy8gRXZlbnRUYXJnZXQgZG9lcyBub3QgaGF2ZSBgZXJyb3JgIGV2ZW50IHNlbWFudGljcyBsaWtlIE5vZGVcbiAgICAvLyBFdmVudEVtaXR0ZXJzLCB3ZSBkbyBub3QgbGlzdGVuIGZvciBgZXJyb3JgIGV2ZW50cyBoZXJlLlxuICAgIGVtaXR0ZXIuYWRkRXZlbnRMaXN0ZW5lcihuYW1lLCBmdW5jdGlvbiB3cmFwTGlzdGVuZXIoYXJnKSB7XG4gICAgICAvLyBJRSBkb2VzIG5vdCBoYXZlIGJ1aWx0aW4gYHsgb25jZTogdHJ1ZSB9YCBzdXBwb3J0IHNvIHdlXG4gICAgICAvLyBoYXZlIHRvIGRvIGl0IG1hbnVhbGx5LlxuICAgICAgaWYgKGZsYWdzLm9uY2UpIHtcbiAgICAgICAgZW1pdHRlci5yZW1vdmVFdmVudExpc3RlbmVyKG5hbWUsIHdyYXBMaXN0ZW5lcik7XG4gICAgICB9XG4gICAgICBsaXN0ZW5lcihhcmcpO1xuICAgIH0pO1xuICB9IGVsc2Uge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1RoZSBcImVtaXR0ZXJcIiBhcmd1bWVudCBtdXN0IGJlIG9mIHR5cGUgRXZlbnRFbWl0dGVyLiBSZWNlaXZlZCB0eXBlICcgKyB0eXBlb2YgZW1pdHRlcik7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29udHJvbGEgbGEgYXBsaWNhY2nDs24gcXVlIG11ZXN0cmEgbHVnYXJlc1xuICovXG5jbGFzcyBQbGFjZXNUcmFja2VyQ29udHJvbGxlciB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge0NhdGVnb3JpZXNWaWV3fSBvcHRpb25zLmNhdGVnb3JpZXNWaWV3XG4gICAqIEBwYXJhbSB7UGxhY2VzVHJhY2tlck1vZGVsfSBvcHRpb25zLnBsYWNlc1RyYWNrZXJNb2RlbFxuICAgKiBAcGFyYW0ge1BsYWNlc0dyaWRWaWV3fSBvcHRpb25zLnBsYWNlc0dyaWRWaWV3XG4gICAqIEBwYXJhbSB7U2VhcmNoQmFyVmlld30gb3B0aW9ucy5zZWFyY2hCYXJWaWV3XG4gICAqIEBwYXJhbSB7UGxhY2VEZXRhaWxzVmlld30gb3B0aW9ucy5wbGFjZURldGFpbHNWaWV3XG4gICAqL1xuXG4gIGNvbnN0cnVjdG9yKHtcbiAgICBjYXRlZ29yaWVzVmlldyxcbiAgICBwbGFjZXNUcmFja2VyTW9kZWwsXG4gICAgcGxhY2VzR3JpZFZpZXcsXG4gICAgc2VhcmNoQmFyVmlldyxcbiAgICBwbGFjZURldGFpbHNWaWV3LFxuICB9KSB7XG4gICAgdGhpcy5fY2F0ZWdvcmllc1ZpZXcgPSBjYXRlZ29yaWVzVmlldztcbiAgICB0aGlzLl9wbGFjZXNUcmFja2VyTW9kZWwgPSBwbGFjZXNUcmFja2VyTW9kZWw7XG4gICAgdGhpcy5fcGxhY2VzR3JpZFZpZXcgPSBwbGFjZXNHcmlkVmlldztcbiAgICB0aGlzLl9wbGFjZURldGFpbHNWaWV3ID0gcGxhY2VEZXRhaWxzVmlldztcbiAgICB0aGlzLl9zZWFyY2hCYXJWaWV3ID0gc2VhcmNoQmFyVmlldztcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmljaWFsaXphIGVsIGNvbXBvbmVudGUgcGFyYSBjYXJnYXIgbG9zIGRhdG9zIGluaWNpYWxlcyB5IGVtcGV6YXIgYSByZWNpYmlyIGV2ZW50b3NcbiAgICovXG4gIHN0YXJ0KCkge1xuICAgIHRoaXMuX3NldHVwQ2F0ZWdvcmllc1ZpZXcoKTtcbiAgICB0aGlzLl9zZXR1cFNlYXJjaEJhclZpZXcoKTtcbiAgICB0aGlzLl9zZXR1cFBsYWNlc0dyaWRWaWV3KCk7XG4gIH1cblxuICAvKipcbiAgICogQ29uZmlndXJhIGxhIHZpc3RhIGRlIGxhcyBkaWZlcmVudGVzIGNhdGVnb3LDrWFzIGRlIGx1Z2FyZXNcbiAgICogIEBwcml2YXRlXG4gICAqL1xuICBfc2V0dXBDYXRlZ29yaWVzVmlldygpIHtcbiAgICB0aGlzLl9jYXRlZ29yaWVzVmlldy5zdGFydCgpO1xuICAgIHRoaXMuX2NhdGVnb3JpZXNWaWV3Lm9uKFwiY2F0ZWdvcnlcIiwgKGRhdGEpID0+XG4gICAgICB0aGlzLl9vblNlbGVjdGluZ0NhdGVnb3J5KGRhdGEpXG4gICAgKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDb25maWd1cmEgbGEgdmlzdGEgZGVsIGdyaWQgZGUgbHVnYXJlc1xuICAgKiAgQHByaXZhdGVcbiAgICovXG4gIF9zZXR1cFBsYWNlc0dyaWRWaWV3KCkge1xuICAgIHRoaXMuX3BsYWNlc0dyaWRWaWV3LnN0YXJ0KCk7XG4gICAgdGhpcy5fcGxhY2VzR3JpZFZpZXcub24oXCJwbGFjZUlkU2VsZWN0ZWRcIiwgKGRhdGEpID0+XG4gICAgICB0aGlzLl9vblNlbGVjdGluZ09uZVBsYWNlKGRhdGEpXG4gICAgKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDb25maWd1cmEgbGEgdmlzdGEgZGUgbGEgYmFycmEgZGUgYsO6c3F1ZWRhIGRlIGx1Z2FyZXNcbiAgICogIEBwcml2YXRlXG4gICAqL1xuICBfc2V0dXBTZWFyY2hCYXJWaWV3KCkge1xuICAgIHRoaXMuX3NlYXJjaEJhclZpZXcuc3RhcnQoKTtcbiAgICB0aGlzLl9zZWFyY2hCYXJWaWV3Lm9uKFwiaW5wdXRcIiwgKGRhdGEpID0+XG4gICAgICB0aGlzLl9vblNlbGVjdGluZ1NlYXJjaEJhcklucHV0KChkYXRhKSlcbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIE11ZXN0cmEgZWwgZ3JpZCBjb24gbGEgaW5mb3JtYWNpw7NuIHF1ZSBub3MgdHJhZSBlbCBtb2RlbG8gYSBwYXJ0aXIgZGUgbGEgZnVuY2nDs24gc2VhcmNoTmVhckJ5TmFtZSgpLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgICAgX29uU2VsZWN0aW5nU2VhcmNoQmFySW5wdXQoeyBpZCB9KSB7XG4gICAgICBjb25zdCBwbGFjZXNBcnJheSA9IFtdO1xuICAgICAgdGhpcy5fcGxhY2VzVHJhY2tlck1vZGVsXG4gICAgICAgIC5zZWFyY2hOZWFyQnlOYW1lKGlkKVxuICAgICAgICAudGhlbigocGxhY2VzKSA9PiB7XG4gICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPD0gNjsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcGxhY2UgPSBwbGFjZXNbaV07XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwbGFjZXMpO1xuICAgICAgICAgICAgaWYgKHBsYWNlLnJhdGluZyA9PSA1KSB7XG4gICAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICAgIGlkOiBwbGFjZS5wbGFjZV9pZCxcbiAgICAgICAgICAgICAgICBuYW1lOiBwbGFjZS5uYW1lLFxuICAgICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgICBwaG90b3M6IHBsYWNlLnBob3Rvc1swXS5nZXRVcmwoKSxcbiAgICAgICAgICAgICAgICByYXRpbmc6IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczM1wiLFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGxhY2UucmF0aW5nID4gNCkge1xuICAgICAgICAgICAgICBwbGFjZXNBcnJheS5wdXNoKHtcbiAgICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgICAgbmFtZTogcGxhY2UubmFtZSxcbiAgICAgICAgICAgICAgICB2aWNpbml0eTogcGxhY2UudmljaW5pdHksXG4gICAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgICAgaHJlZjogbnVsbCxcbiAgICAgICAgICAgICAgICByYXRpbmc6IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNFwiLFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGxhY2UucmF0aW5nID4gMykge1xuICAgICAgICAgICAgICBwbGFjZXNBcnJheS5wdXNoKHtcbiAgICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgICAgbmFtZTogcGxhY2UubmFtZSxcbiAgICAgICAgICAgICAgICB2aWNpbml0eTogcGxhY2UudmljaW5pdHksXG4gICAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgICAgcmF0aW5nOiBcIiYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzRcIixcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDIpIHtcbiAgICAgICAgICAgICAgcGxhY2VzQXJyYXkucHVzaCh7XG4gICAgICAgICAgICAgICAgaWQ6IHBsYWNlLnBsYWNlX2lkLFxuICAgICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgICAgdmljaW5pdHk6IHBsYWNlLnZpY2luaXR5LFxuICAgICAgICAgICAgICAgIHBob3RvczogcGxhY2UucGhvdG9zWzBdLmdldFVybCgpLFxuICAgICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCIsXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChwbGFjZS5yYXRpbmcgPiAxKSB7XG4gICAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICAgIGlkOiBwbGFjZS5wbGFjZV9pZCxcbiAgICAgICAgICAgICAgICBuYW1lOiBwbGFjZS5uYW1lLFxuICAgICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgICBwaG90b3M6IHBsYWNlLnBob3Rvc1swXS5nZXRVcmwoKSxcbiAgICAgICAgICAgICAgICByYXRpbmc6IFwiJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiLFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGxhY2UucmF0aW5nID4gMCkge1xuICAgICAgICAgICAgICBwbGFjZXNBcnJheS5wdXNoKHtcbiAgICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgICAgbmFtZTogcGxhY2UubmFtZSxcbiAgICAgICAgICAgICAgICB2aWNpbml0eTogcGxhY2UudmljaW5pdHksXG4gICAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKHsgbWF4SGVpZ2h0OiAzMCB9KSxcbiAgICAgICAgICAgICAgICByYXRpbmc6IFwiJiM5NzM0ICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiLFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5fcGxhY2VzR3JpZFZpZXcuc2hvdyhwbGFjZXNBcnJheSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgcmV0dXJuIGFsZXJ0KFwiTm8gc2UgaGFuIGVuY29udHJhZG8gcmVzdWx0YWRvc1wiKTtcbiAgICAgICAgfSk7XG4gICAgfVxuICBcblxuICAvL0hlIGVuY29udHJhZG8gdW4gcHJvYmxlbWEgYWwgaW50ZW50YXIgb2J0ZW5lciBlbCBib29sZWFubyBkZSBvcGVuX25vdywgcGFyYVxuICAvL3BvbmVyIHNpIGVzdMOhIGFiaWVydG8gbyBubyBlbCBzaXRpby5cblxuICAvL0hlIHZpc3RvIHF1ZSBcIm9wZW5fbm93XCIgeSBcInV0Y19vZmZzZXRcIiBlc3TDoW4gb2Jzb2xldG9zIGVuIGxhIGRvY3VtZW50YWNpw7NuIGRlIFBsYWNlcyBMaWJyYXJ5LlxuXG4gIC8qKlxuICAgKiBNdWVzdHJhIGxhIHNlY2Npw7NuIGRlIFBsYWNlIGRldGFpbHMgY29uIGxhIGluZm9ybWFjacOzbiBkZWwgbW9kZWxvIGEgcGFydGlyIGRlIGxhIGZ1bmNpw7NuIHBsYWNlRGV0YWlscygpLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX29uU2VsZWN0aW5nT25lUGxhY2UoeyBpZCB9KSB7XG4gICAgY29uc3QgZGV0YWlsc1BsYWNlQXJyYXkgPSBbXTtcbiAgICB0aGlzLl9wbGFjZXNUcmFja2VyTW9kZWxcbiAgICAgIC5wbGFjZURldGFpbHMoaWQpXG4gICAgICAudGhlbigoZGV0YWlscykgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhkZXRhaWxzKTtcbiAgICAgICAgLy8gRXN0cmVsbGFzIHBhcmEgbG9zIGNvbWVudGFyaW9zXG4gICAgICAgIGlmIChkZXRhaWxzLnJldmlld3NbMF0ucmF0aW5nID09IDUpIHtcbiAgICAgICAgICB2YXIgc3RhcnMxID0gXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzXCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzBdLnJhdGluZyA+PSA0KSB7XG4gICAgICAgICAgdmFyIHN0YXJzMSA9IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1swXS5yYXRpbmcgPj0gMykge1xuICAgICAgICAgIHZhciBzdGFyczEgPSBcIiYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfSBlbHNlIGlmIChkZXRhaWxzLnJldmlld3NbMF0ucmF0aW5nID49IDIpIHtcbiAgICAgICAgICB2YXIgc3RhcnMxID0gXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzBdLnJhdGluZyA+PSAxKSB7XG4gICAgICAgICAgdmFyIHN0YXJzMSA9IFwiJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1swXS5yYXRpbmcgPj0gMCkge1xuICAgICAgICAgIHZhciBzdGFyczEgPSBcIiYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkZXRhaWxzLnJldmlld3NbMV0ucmF0aW5nID09IDUpIHtcbiAgICAgICAgICB2YXIgc3RhcnMyID0gXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzXCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzFdLnJhdGluZyA+PSA0KSB7XG4gICAgICAgICAgdmFyIHN0YXJzMiA9IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1sxXS5yYXRpbmcgPj0gMykge1xuICAgICAgICAgIHZhciBzdGFyczIgPSBcIiYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfSBlbHNlIGlmIChkZXRhaWxzLnJldmlld3NbMV0ucmF0aW5nID49IDIpIHtcbiAgICAgICAgICB2YXIgc3RhcnMyID0gXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzFdLnJhdGluZyA+PSAxKSB7XG4gICAgICAgICAgdmFyIHN0YXJzMiA9IFwiJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1sxXS5yYXRpbmcgPj0gMCkge1xuICAgICAgICAgIHZhciBzdGFyczIgPSBcIiYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkZXRhaWxzLnJldmlld3NbMl0ucmF0aW5nID09IDUpIHtcbiAgICAgICAgICB2YXIgc3RhcnMzID0gXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzXCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzJdLnJhdGluZyA+PSA0KSB7XG4gICAgICAgICAgdmFyIHN0YXJzMyA9IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1syXS5yYXRpbmcgPj0gMykge1xuICAgICAgICAgIHZhciBzdGFyczMgPSBcIiYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfSBlbHNlIGlmIChkZXRhaWxzLnJldmlld3NbMl0ucmF0aW5nID49IDIpIHtcbiAgICAgICAgICB2YXIgc3RhcnMzID0gXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCI7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yZXZpZXdzWzJdLnJhdGluZyA+PSAxKSB7XG4gICAgICAgICAgdmFyIHN0YXJzMyA9IFwiJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmV2aWV3c1syXS5yYXRpbmcgPj0gMCkge1xuICAgICAgICAgIHZhciBzdGFyczMgPSBcIiYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNCAmIzk3MzRcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkZXRhaWxzLnJhdGluZyA9PSA1KSB7XG4gICAgICAgICAgZGV0YWlsc1BsYWNlQXJyYXkucHVzaCh7XG4gICAgICAgICAgICBwaG90b1BsYWNlOiBkZXRhaWxzLnBob3Rvc1swXS5nZXRVcmwoKSxcbiAgICAgICAgICAgIG5hbWVQbGFjZTogZGV0YWlscy5uYW1lLFxuICAgICAgICAgICAgYWRyZXNzUGxhY2U6IGRldGFpbHMuZm9ybWF0dGVkX2FkZHJlc3MsXG4gICAgICAgICAgICBwaG9uZVBsYWNlOiBkZXRhaWxzLmZvcm1hdHRlZF9waG9uZV9udW1iZXIsXG4gICAgICAgICAgICByYXRpbmdQbGFjZTogXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzXCIsXG4gICAgICAgICAgICB3ZWJzaXRlUGxhY2U6IGRldGFpbHMud2Vic2l0ZSxcblxuICAgICAgICAgICAgLy9SZXZpZXcgMVxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMUF1dGhvcjogZGV0YWlscy5yZXZpZXdzWzBdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVRleHQ6IGRldGFpbHMucmV2aWV3c1swXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVRpbWU6IGRldGFpbHMucmV2aWV3c1swXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVJhdGluZzogZGV0YWlscy5yZXZpZXdzWzBdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFTdGFyczogc3RhcnMxLFxuXG4gICAgICAgICAgICAvL1JldmlldyAyXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMV0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyVGV4dDogZGV0YWlscy5yZXZpZXdzWzFdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyVGltZTogZGV0YWlscy5yZXZpZXdzWzFdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMV0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlN0YXJzOiBzdGFyczIsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDJcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNBdXRob3I6IGRldGFpbHMucmV2aWV3c1syXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNUZXh0OiBkZXRhaWxzLnJldmlld3NbMl0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNUaW1lOiBkZXRhaWxzLnJldmlld3NbMl0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNSYXRpbmc6IGRldGFpbHMucmV2aWV3c1syXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzU3RhcnM6IHN0YXJzMyxcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChkZXRhaWxzLnJhdGluZyA+IDQpIHtcbiAgICAgICAgICBkZXRhaWxzUGxhY2VBcnJheS5wdXNoKHtcbiAgICAgICAgICAgIHBob3RvUGxhY2U6IGRldGFpbHMucGhvdG9zWzBdLmdldFVybCgpLFxuICAgICAgICAgICAgbmFtZVBsYWNlOiBkZXRhaWxzLm5hbWUsXG4gICAgICAgICAgICBhZHJlc3NQbGFjZTogZGV0YWlscy5mb3JtYXR0ZWRfYWRkcmVzcyxcbiAgICAgICAgICAgIHBob25lUGxhY2U6IGRldGFpbHMuZm9ybWF0dGVkX3Bob25lX251bWJlcixcbiAgICAgICAgICAgIHJhdGluZ1BsYWNlOiBcIiYjOTczMyAmIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzRcIixcbiAgICAgICAgICAgIHdlYnNpdGVQbGFjZTogZGV0YWlscy53ZWJzaXRlLFxuXG4gICAgICAgICAgICAvL1JldmlldyAxXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMF0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxVGV4dDogZGV0YWlscy5yZXZpZXdzWzBdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxVGltZTogZGV0YWlscy5yZXZpZXdzWzBdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMF0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVN0YXJzOiBzdGFyczEsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDJcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJBdXRob3I6IGRldGFpbHMucmV2aWV3c1sxXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJUZXh0OiBkZXRhaWxzLnJldmlld3NbMV0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJUaW1lOiBkZXRhaWxzLnJldmlld3NbMV0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJSYXRpbmc6IGRldGFpbHMucmV2aWV3c1sxXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyU3RhcnM6IHN0YXJzMixcblxuICAgICAgICAgICAgLy9SZXZpZXcgMlxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM0F1dGhvcjogZGV0YWlscy5yZXZpZXdzWzJdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1RleHQ6IGRldGFpbHMucmV2aWV3c1syXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1RpbWU6IGRldGFpbHMucmV2aWV3c1syXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1JhdGluZzogZGV0YWlscy5yZXZpZXdzWzJdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNTdGFyczogc3RhcnMzLFxuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmF0aW5nID4gMykge1xuICAgICAgICAgIGRldGFpbHNQbGFjZUFycmF5LnB1c2goe1xuICAgICAgICAgICAgcGhvdG9QbGFjZTogZGV0YWlscy5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICBuYW1lUGxhY2U6IGRldGFpbHMubmFtZSxcbiAgICAgICAgICAgIGFkcmVzc1BsYWNlOiBkZXRhaWxzLmZvcm1hdHRlZF9hZGRyZXNzLFxuICAgICAgICAgICAgcGhvbmVQbGFjZTogZGV0YWlscy5mb3JtYXR0ZWRfcGhvbmVfbnVtYmVyLFxuICAgICAgICAgICAgcmF0aW5nUGxhY2U6IFwiJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzM0ICYjOTczNFwiLFxuICAgICAgICAgICAgd2Vic2l0ZVBsYWNlOiBkZXRhaWxzLndlYnNpdGUsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDFcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFBdXRob3I6IGRldGFpbHMucmV2aWV3c1swXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFUZXh0OiBkZXRhaWxzLnJldmlld3NbMF0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFUaW1lOiBkZXRhaWxzLnJldmlld3NbMF0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFSYXRpbmc6IGRldGFpbHMucmV2aWV3c1swXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxU3RhcnM6IHN0YXJzMSxcblxuICAgICAgICAgICAgLy9SZXZpZXcgMlxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMkF1dGhvcjogZGV0YWlscy5yZXZpZXdzWzFdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlRleHQ6IGRldGFpbHMucmV2aWV3c1sxXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlRpbWU6IGRldGFpbHMucmV2aWV3c1sxXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlJhdGluZzogZGV0YWlscy5yZXZpZXdzWzFdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJTdGFyczogc3RhcnMyLFxuXG4gICAgICAgICAgICAvL1JldmlldyAyXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMl0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzVGV4dDogZGV0YWlscy5yZXZpZXdzWzJdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzVGltZTogZGV0YWlscy5yZXZpZXdzWzJdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMl0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1N0YXJzOiBzdGFyczMsXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSBpZiAoZGV0YWlscy5yYXRpbmcgPiAyKSB7XG4gICAgICAgICAgZGV0YWlsc1BsYWNlQXJyYXkucHVzaCh7XG4gICAgICAgICAgICBwaG90b1BsYWNlOiBkZXRhaWxzLnBob3Rvc1swXS5nZXRVcmwoKSxcbiAgICAgICAgICAgIG5hbWVQbGFjZTogZGV0YWlscy5uYW1lLFxuICAgICAgICAgICAgYWRyZXNzUGxhY2U6IGRldGFpbHMuZm9ybWF0dGVkX2FkZHJlc3MsXG4gICAgICAgICAgICBwaG9uZVBsYWNlOiBkZXRhaWxzLmZvcm1hdHRlZF9waG9uZV9udW1iZXIsXG4gICAgICAgICAgICByYXRpbmdQbGFjZTogXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCIsXG4gICAgICAgICAgICB3ZWJzaXRlUGxhY2U6IGRldGFpbHMud2Vic2l0ZSxcblxuICAgICAgICAgICAgLy9SZXZpZXcgMVxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMUF1dGhvcjogZGV0YWlscy5yZXZpZXdzWzBdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVRleHQ6IGRldGFpbHMucmV2aWV3c1swXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVRpbWU6IGRldGFpbHMucmV2aWV3c1swXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVJhdGluZzogZGV0YWlscy5yZXZpZXdzWzBdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFTdGFyczogc3RhcnMxLFxuXG4gICAgICAgICAgICAvL1JldmlldyAyXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMV0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyVGV4dDogZGV0YWlscy5yZXZpZXdzWzFdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyVGltZTogZGV0YWlscy5yZXZpZXdzWzFdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMV0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlN0YXJzOiBzdGFyczIsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDJcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNBdXRob3I6IGRldGFpbHMucmV2aWV3c1syXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNUZXh0OiBkZXRhaWxzLnJldmlld3NbMl0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNUaW1lOiBkZXRhaWxzLnJldmlld3NbMl0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNSYXRpbmc6IGRldGFpbHMucmV2aWV3c1syXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzU3RhcnM6IHN0YXJzMyxcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChkZXRhaWxzLnJhdGluZyA+IDEpIHtcbiAgICAgICAgICBkZXRhaWxzUGxhY2VBcnJheS5wdXNoKHtcbiAgICAgICAgICAgIHBob3RvUGxhY2U6IGRldGFpbHMucGhvdG9zWzBdLmdldFVybCgpLFxuICAgICAgICAgICAgbmFtZVBsYWNlOiBkZXRhaWxzLm5hbWUsXG4gICAgICAgICAgICBhZHJlc3NQbGFjZTogZGV0YWlscy5mb3JtYXR0ZWRfYWRkcmVzcyxcbiAgICAgICAgICAgIHBob25lUGxhY2U6IGRldGFpbHMuZm9ybWF0dGVkX3Bob25lX251bWJlcixcbiAgICAgICAgICAgIHJhdGluZ1BsYWNlOiBcIiYjOTczMyAmIzk3MzQgJiM5NzM0ICYjOTczNCAmIzk3MzRcIixcbiAgICAgICAgICAgIHdlYnNpdGVQbGFjZTogZGV0YWlscy53ZWJzaXRlLFxuXG4gICAgICAgICAgICAvL1JldmlldyAxXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMF0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxVGV4dDogZGV0YWlscy5yZXZpZXdzWzBdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxVGltZTogZGV0YWlscy5yZXZpZXdzWzBdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMF0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMVN0YXJzOiBzdGFyczEsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDJcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJBdXRob3I6IGRldGFpbHMucmV2aWV3c1sxXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJUZXh0OiBkZXRhaWxzLnJldmlld3NbMV0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJUaW1lOiBkZXRhaWxzLnJldmlld3NbMV0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJSYXRpbmc6IGRldGFpbHMucmV2aWV3c1sxXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UyU3RhcnM6IHN0YXJzMixcblxuICAgICAgICAgICAgLy9SZXZpZXcgMlxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM0F1dGhvcjogZGV0YWlscy5yZXZpZXdzWzJdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1RleHQ6IGRldGFpbHMucmV2aWV3c1syXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1RpbWU6IGRldGFpbHMucmV2aWV3c1syXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1JhdGluZzogZGV0YWlscy5yZXZpZXdzWzJdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTNTdGFyczogc3RhcnMzLFxuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2UgaWYgKGRldGFpbHMucmF0aW5nID4gMCkge1xuICAgICAgICAgIGRldGFpbHNQbGFjZUFycmF5LnB1c2goe1xuICAgICAgICAgICAgcGhvdG9QbGFjZTogZGV0YWlscy5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICBuYW1lUGxhY2U6IGRldGFpbHMubmFtZSxcbiAgICAgICAgICAgIGFkcmVzc1BsYWNlOiBkZXRhaWxzLmZvcm1hdHRlZF9hZGRyZXNzLFxuICAgICAgICAgICAgcGhvbmVQbGFjZTogZGV0YWlscy5mb3JtYXR0ZWRfcGhvbmVfbnVtYmVyLFxuICAgICAgICAgICAgcmF0aW5nUGxhY2U6IFwiJiM5NzM0ICYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNFwiLFxuICAgICAgICAgICAgd2Vic2l0ZVBsYWNlOiBkZXRhaWxzLndlYnNpdGUsXG5cbiAgICAgICAgICAgIC8vUmV2aWV3IDFcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFBdXRob3I6IGRldGFpbHMucmV2aWV3c1swXS5hdXRob3JfbmFtZSxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFUZXh0OiBkZXRhaWxzLnJldmlld3NbMF0udGV4dCxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFUaW1lOiBkZXRhaWxzLnJldmlld3NbMF0ucmVsYXRpdmVfdGltZV9kZXNjcmlwdGlvbixcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTFSYXRpbmc6IGRldGFpbHMucmV2aWV3c1swXS5yYXRpbmcsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UxU3RhcnM6IHN0YXJzMSxcblxuICAgICAgICAgICAgLy9SZXZpZXcgMlxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMkF1dGhvcjogZGV0YWlscy5yZXZpZXdzWzFdLmF1dGhvcl9uYW1lLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlRleHQ6IGRldGFpbHMucmV2aWV3c1sxXS50ZXh0LFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlRpbWU6IGRldGFpbHMucmV2aWV3c1sxXS5yZWxhdGl2ZV90aW1lX2Rlc2NyaXB0aW9uLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlMlJhdGluZzogZGV0YWlscy5yZXZpZXdzWzFdLnJhdGluZyxcbiAgICAgICAgICAgIHJldmlld3NQbGFjZTJTdGFyczogc3RhcnMyLFxuXG4gICAgICAgICAgICAvL1JldmlldyAyXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzQXV0aG9yOiBkZXRhaWxzLnJldmlld3NbMl0uYXV0aG9yX25hbWUsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzVGV4dDogZGV0YWlscy5yZXZpZXdzWzJdLnRleHQsXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzVGltZTogZGV0YWlscy5yZXZpZXdzWzJdLnJlbGF0aXZlX3RpbWVfZGVzY3JpcHRpb24sXG4gICAgICAgICAgICByZXZpZXdzUGxhY2UzUmF0aW5nOiBkZXRhaWxzLnJldmlld3NbMl0ucmF0aW5nLFxuICAgICAgICAgICAgcmV2aWV3c1BsYWNlM1N0YXJzOiBzdGFyczMsXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc29sZS5sb2coZGV0YWlsc1BsYWNlQXJyYXkpO1xuICAgICAgICB0aGlzLl9wbGFjZURldGFpbHNWaWV3LnNob3coZGV0YWlsc1BsYWNlQXJyYXkpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICByZXR1cm4gYWxlcnQoXCJObyBzZSBoYW4gZW5jb250cmFkbyByZXN1bHRhZG9zXCIpO1xuICAgICAgfSk7XG4gIH1cblxuXG4gIC8qKlxuICAgKiBNdWVzdHJhIGVsIGdyaWQgY29uIGxhIGluZm9ybWFjacOzbiBxdWUgbm9zIHRyYWUgZWwgbW9kZWxvIGEgcGFydGlyIGRlIGxhIGZ1bmNpw7NuIHNlYXJjaE5lYXJCeVR5cGUoKS5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9vblNlbGVjdGluZ0NhdGVnb3J5KHsgaWQgfSkge1xuICAgIGNvbnN0IHBsYWNlc0FycmF5ID0gW107XG4gICAgdGhpcy5fcGxhY2VzVHJhY2tlck1vZGVsXG4gICAgICAuc2VhcmNoTmVhckJ5VHlwZShpZClcbiAgICAgIC50aGVuKChwbGFjZXMpID0+IHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCA2OyBpKyspIHtcbiAgICAgICAgICBsZXQgcGxhY2UgPSBwbGFjZXNbaV07XG4gICAgICAgICAgY29uc29sZS5sb2cocGxhY2VzKTtcbiAgICAgICAgICBpZiAocGxhY2UucmF0aW5nID09IDUpIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzMzXCIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDQpIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgIGhyZWY6IG51bGwsXG4gICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzMgJiM5NzM0XCIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDMpIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzMzICYjOTczMyAmIzk3MzQgJiM5NzM0XCIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDIpIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzMzICYjOTczNCAmIzk3MzQgJiM5NzM0XCIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDEpIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKCksXG4gICAgICAgICAgICAgIHJhdGluZzogXCImIzk3MzMgJiM5NzM0ICYjOTczNCAmIzk3MzQgJiM5NzM0XCIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHBsYWNlLnJhdGluZyA+IDApIHtcbiAgICAgICAgICAgIHBsYWNlc0FycmF5LnB1c2goe1xuICAgICAgICAgICAgICBpZDogcGxhY2UucGxhY2VfaWQsXG4gICAgICAgICAgICAgIG5hbWU6IHBsYWNlLm5hbWUsXG4gICAgICAgICAgICAgIHZpY2luaXR5OiBwbGFjZS52aWNpbml0eSxcbiAgICAgICAgICAgICAgcGhvdG9zOiBwbGFjZS5waG90b3NbMF0uZ2V0VXJsKHsgbWF4SGVpZ2h0OiAzMCB9KSxcbiAgICAgICAgICAgICAgcmF0aW5nOiBcIiYjOTczNCAmIzk3MzQgJiM5NzM0ICYjOTczNCAmIzk3MzRcIixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9wbGFjZXNHcmlkVmlldy5zaG93KHBsYWNlc0FycmF5KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgcmV0dXJuIGFsZXJ0KFwiTm8gc2UgaGFuIGVuY29udHJhZG8gcmVzdWx0YWRvc1wiKTtcbiAgICAgIH0pO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBsYWNlc1RyYWNrZXJDb250cm9sbGVyO1xuIiwiY2xhc3MgUGxhY2VzVHJhY2tlck1vZGVsIHtcbiAgc2VhcmNoTmVhckJ5VHlwZShjYXRlZ29yeSkge1xuICAgIGNvbnNvbGUubG9nKGNhdGVnb3J5KTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3Qgc2VydmljZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VzU2VydmljZShcbiAgICAgICAgZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKVxuICAgICAgKTtcbiAgICAgIGxldCByZXF1ZXN0ID0ge1xuICAgICAgICBsb2NhdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyg0MS40MTE1NDksIDIuMTk0MTE4KSxcbiAgICAgICAgcmFkaXVzOiA1MDAwMCxcbiAgICAgICAgdHlwZTogW2NhdGVnb3J5XSxcbiAgICAgIH07XG5cbiAgICAgIHNlcnZpY2UubmVhcmJ5U2VhcmNoKHJlcXVlc3QsIHJlc29sdmUpO1xuICAgIH0pO1xuICB9XG5cbiAgc2VhcmNoTmVhckJ5TmFtZShzZWFyY2gpIHtcbiAgICBjb25zb2xlLmxvZyhzZWFyY2gpO1xuICAgIC8vIHNlYXJjaCA9ICdtYXF1aW5pc3RhJztcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgY29uc3Qgc2VydmljZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VzU2VydmljZShcbiAgICAgICAgZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKVxuICAgICAgKTtcbiAgICAgIGxldCByZXF1ZXN0ID0ge1xuICAgICAgICBsb2NhdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyg0MS40MTE1NDksIDIuMTk0MTE4KSxcbiAgICAgICAgcmFkaXVzOiA1MDAwMCxcbiAgICAgICAga2V5d29yZDogc2VhcmNoLFxuICAgICAgfTtcbiAgICAgIHNlcnZpY2UubmVhcmJ5U2VhcmNoKHJlcXVlc3QsIHJlc29sdmUpO1xuICAgIH0pO1xuICB9XG5cbiAgcGxhY2VEZXRhaWxzKHBsYWNlX2lkKSB7XG4gICAgY29uc29sZS5sb2cocGxhY2VfaWQpO1xuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IHNlcnZpY2UgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlc1NlcnZpY2UoXG4gICAgICAgIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgICAgICk7XG4gICAgICBsZXQgcmVxdWVzdCA9IHtcbiAgICAgICAgcGxhY2VJZDogcGxhY2VfaWQsXG4gICAgICAgIGZpZWxkczogW1xuICAgICAgICAgIFwicGhvdG9cIixcbiAgICAgICAgICBcIm5hbWVcIixcbiAgICAgICAgICBcImZvcm1hdHRlZF9hZGRyZXNzXCIsXG4gICAgICAgICAgXCJmb3JtYXR0ZWRfcGhvbmVfbnVtYmVyXCIsXG4gICAgICAgICAgXCJyYXRpbmdcIixcbiAgICAgICAgICBcIndlYnNpdGVcIixcbiAgICAgICAgICBcInJldmlld3NcIixcbiAgICAgICAgICBcInR5cGVcIixcbiAgICAgICAgXSxcbiAgICAgIH07XG5cbiAgICAgIHNlcnZpY2UuZ2V0RGV0YWlscyhyZXF1ZXN0LCByZXNvbHZlKTtcbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBQbGFjZXNUcmFja2VyTW9kZWw7XG4iLCJcbi8qKlxuICogSW5kaWNhIHNpIGVsIGVsZW1lbnRvIGluZGljYWRvIGN1bXBsZSBjb24gZWwgc2VsZWN0b3IuXG4gKiBAcGFyYW0gIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0gIHtzdHJpbmd9IHNlbGVjdG9yXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBtYXRjaGVzKGVsZW1lbnQsIHNlbGVjdG9yKSB7XG4gIHJldHVybiBlbGVtZW50Lm1hdGNoZXMoc2VsZWN0b3IpO1xufVxuXG4vKipcbiAqIEVqZWN1dGEgYGhhbmRsZXJgIGN1YW5kbyBvY3VycmUgZWwgZXZlbnRvIGNvbiBub21icmUgYGV2ZW50TmFtZWBcbiAqIGVuIHVuIGVsZW1lbnRvIGNvbnRlbmlkbyBlbiBgY29udGFpbmVyYCBxdWUgY3VtcGxlIGNvbiBlbCBzZWxlY3RvciBgdGFyZ2V0U2VsZWN0b3JgXG4gKiBAcGFyYW0gIHtFbGVtZW50fSBjb250YWluZXJcbiAqIEBwYXJhbSAge3N0cmluZ30gZXZlbnROYW1lXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHRhcmdldFNlbGVjdG9yXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gaGFuZGxlclxuICovXG5mdW5jdGlvbiBkZWxlZ2F0ZUV2ZW50KGNvbnRhaW5lciwgZXZlbnROYW1lLCB0YXJnZXRTZWxlY3RvciwgaGFuZGxlcikge1xuICBjb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcihldmVudE5hbWUsIChldmVudCkgPT4ge1xuICAgIGxldCB0YXJnZXQgPSBldmVudC50YXJnZXQ7XG4gICAgd2hpbGUgKHRhcmdldCAhPT0gY29udGFpbmVyICYmICFtYXRjaGVzKHRhcmdldCwgdGFyZ2V0U2VsZWN0b3IpKSB7XG4gICAgICB0YXJnZXQgPSB0YXJnZXQucGFyZW50Tm9kZTtcbiAgICB9XG4gICAgaWYgKHRhcmdldCAhPT0gY29udGFpbmVyKSB7XG4gICAgICBldmVudC5kZWxlZ2F0ZVRhcmdldCA9IHRhcmdldDtcbiAgICAgIGhhbmRsZXIoZXZlbnQpO1xuICAgIH1cbiAgfSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGRlbGVnYXRlRXZlbnQ7XG4iLCIvKipcbiAqIERldnVlbHZlIGxhIHJlcHJlc2VudGFjacOzbiBlbiBmb3JtYSBkZSBlbGVtZW50byBIVE1MIGEgcGFydGlyIGRlbCBzdHJpbmdcbiAqIGluZGljYWRvLiBzdHJpbmcgPT4gRWxlbWVudFxuICogQHBhcmFtICB7c3RyaW5nfSBodG1sXG4gKiBAcmV0dXJuIHtFbGVtZW50fVxuICovXG5mdW5jdGlvbiBodG1sVG9FbGVtZW50KGh0bWwpIHtcbiAgY29uc3Qgd3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICB3cmFwcGVyLmlubmVySFRNTCA9IGh0bWw7XG4gIGNvbnN0IGVsZW1lbnQgPSB3cmFwcGVyLmZpcnN0RWxlbWVudENoaWxkO1xuICByZXR1cm4gZWxlbWVudDtcbn1cblxuZXhwb3J0IGRlZmF1bHQgaHRtbFRvRWxlbWVudDtcbiIsImltcG9ydCBkZWxlZ2F0ZUV2ZW50IGZyb20gXCIuLi91dGlscy9kZWxlZ2F0ZUV2ZW50XCI7XG5cbnZhciBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKFwiZXZlbnRzXCIpO1xuXG4vKipcbiAqIEdlc3Rpb25hIGxhIHZpc3RhIGRlIGxhcyBjYXRlZ29yaWFzLlxuICogRW1pdGUgZXZlbnRvcyBjdWFuZG8gc2Ugc2VsZWNjaW9uYSB1bmEgY2F0ZWdvcsOtYS5cbiAqL1xuY2xhc3MgQ2F0ZWdvcmllc1ZpZXcgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBvcHRpb25zLmVsZW1lbnRcbiAgICovXG4gIGNvbnN0cnVjdG9yKHsgZWxlbWVudCB9KSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudDtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmljaWEgZWwgY29tcG9uZW50ZVxuICAgKi9cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5fc2V0dXBTZWxlY3RDYXRlZ29yeSgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEluaWNpYSBlbCBldmVudG8gcGFyYSBzZWxlY2Npb25hciBsYSBjYXRlZ29yw61hXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfc2V0dXBTZWxlY3RDYXRlZ29yeSgpIHtcbiAgICBkZWxlZ2F0ZUV2ZW50KHRoaXMuX2VsZW1lbnQsIFwiY2xpY2tcIiwgXCJbY2F0ZWdvcnktaWRdXCIsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgaWQgPSBldmVudC5kZWxlZ2F0ZVRhcmdldC5nZXRBdHRyaWJ1dGUoXCJjYXRlZ29yeS1pZFwiKTtcbiAgICAgIHRoaXMuZW1pdChcImNhdGVnb3J5XCIsIHsgaWQgfSk7XG4gICAgfSk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQ2F0ZWdvcmllc1ZpZXc7XG4iLCJpbXBvcnQgaHRtbFRvRWxlbWVudCBmcm9tIFwiLi4vdXRpbHMvaHRtbFRvRWxlbWVudFwiO1xuXG52YXIgRXZlbnRFbWl0dGVyID0gcmVxdWlyZShcImV2ZW50c1wiKTtcblxuLyoqXG4gKiBHZXN0aW9uYSBsYSB2aXN0YSBkZWwgbHVnYXIgbcOhcyBkZXRhbGxhZG8uXG4gKi9cbmNsYXNzIFBsYWNlRGV0YWlsc1ZpZXcgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xuICAvKipcbiAgICogQHBhcmFtIHtFbGVtZW50fSBvcHRpb25zLmVsZW1lbnRcbiAgICovXG5cbiAgY29uc3RydWN0b3IoeyBlbGVtZW50IH0pIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xuICB9XG5cbiAgLyoqXG4gICAqIE11ZXN0cmEgbG9zIGRldGFsbGVzIGRlbCBsdWdhciBzZWxlY2Npb25hZG9cbiAgICogQHBhcmFtICB7T2JqZWN0fSBkZXRhaWxzXG4gICAqL1xuICBzaG93KGRldGFpbHMpIHtcbiAgICB0aGlzLl9lbGVtZW50LmlubmVySFRNTCA9IFwiXCI7XG4gICAgZGV0YWlscy5mb3JFYWNoKChkZXRhaWwpID0+XG4gICAgICB0aGlzLl9lbGVtZW50LmFwcGVuZENoaWxkKHJlbmRlclBsYWNlRGV0YWlscyhkZXRhaWwpKVxuICAgICk7XG4gIH1cbn1cblxuLyoqXG4gKiBEZXZ1ZWx2ZSB1biBlbGVtZW50byBIVE1MIHF1ZSByZXByZXNlbnRhIGxvcyBkZXRhbGxlcyBkZWwgbHVnYXIgc2VsZWNjaW9uYWRvXG4gKiBAcGFyYW0gIHtPYmplY3R9IGRldGFpbFxuICogQHJldHVybiB7RWxlbWVudH1cbiAqL1xuZnVuY3Rpb24gcmVuZGVyUGxhY2VEZXRhaWxzKGRldGFpbCkge1xuICByZXR1cm4gaHRtbFRvRWxlbWVudChgXG4gICAgPGRpdj5cbiAgICAgIDxoMiBjbGFzcz1cIm15LTRcIj5QbGFjZSBkZXRhaWxzPC9oMj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjYXJkIG10LTRcIj5cbiAgICAgICAgPGltZyBjbGFzcz1cImNhcmQtaW1nLXRvcCBpbWctZmx1aWRcIiBzcmM9XCIke2RldGFpbC5waG90b1BsYWNlfVwiIGFsdD1cIlwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiY2FyZC1ib2R5XCI+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxoMyBjbGFzcz1cImNhcmQtdGl0bGVcIj4ke2RldGFpbC5uYW1lUGxhY2V9PC9oMz5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGV4dC13YXJuaW5nXCIgc3R5bGU9XCJmbG9hdDpyaWdodDtcIj4ke2RldGFpbC5yYXRpbmdQbGFjZX08L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICA8c3Ryb25nPiR7ZGV0YWlsLmFkcmVzc1BsYWNlfTwvc3Ryb25nPlxuICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8dWw+XG4gICAgICAgICAgICA8bGk+JHtkZXRhaWwucGhvbmVQbGFjZX08L2xpPlxuICAgICAgICAgICAgPGxpPjxzcGFuIGNsYXNzPVwidGV4dC1zdWNjZXNzXCI+T3Blbjwvc3Bhbj4gLSA8c3BhbiBjbGFzcz1cInRleHQtZGFuZ2VyXCI+Q2xvc2U8L3NwYW4+PC9saT5cbiAgICAgICAgICAgIDxsaT48YSBocmVmPVwiJHtkZXRhaWwud2Vic2l0ZVBsYWNlfVwiPldlYnNpdGU8L2E+PC9saT5cbiAgICAgICAgICA8L3VsPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuXG4gICAgICA8ZGl2IGNsYXNzPVwiY2FyZCBjYXJkLW91dGxpbmUtc2Vjb25kYXJ5IG15LTRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtaGVhZGVyXCI+XG4gICAgICAgICAgUGxhY2UgUmV2aWV3c1xuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGV4dC13YXJuaW5nXCI+JHtkZXRhaWwucmV2aWV3c1BsYWNlMVN0YXJzfTwvc3Bhbj5cbiAgICAgICAgICAke2RldGFpbC5yZXZpZXdzUGxhY2UxUmF0aW5nfSBzdGFyc1xuICAgICAgICAgIDxwPiR7ZGV0YWlsLnJldmlld3NQbGFjZTFUZXh0fTwvcD5cbiAgICAgICAgICA8c21hbGwgY2xhc3M9XCJ0ZXh0LW11dGVkXCI+JHtkZXRhaWwucmV2aWV3c1BsYWNlMUF1dGhvcn0gJHtkZXRhaWwucmV2aWV3c1BsYWNlMVRpbWV9PC9zbWFsbD5cbiAgICAgICAgICA8aHI+XG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0ZXh0LXdhcm5pbmdcIj4ke2RldGFpbC5yZXZpZXdzUGxhY2UyU3RhcnN9PC9zcGFuPlxuICAgICAgICAgICR7ZGV0YWlsLnJldmlld3NQbGFjZTJSYXRpbmd9IHN0YXJzXG4gICAgICAgICAgPHA+JHtkZXRhaWwucmV2aWV3c1BsYWNlMlRleHR9PC9wPlxuICAgICAgICAgIDxzbWFsbCBjbGFzcz1cInRleHQtbXV0ZWRcIj4ke2RldGFpbC5yZXZpZXdzUGxhY2UyQXV0aG9yfSAke2RldGFpbC5yZXZpZXdzUGxhY2UxVGltZX08L3NtYWxsPlxuICAgICAgICAgIDxocj5cbiAgICAgICAgICA8c3BhbiBjbGFzcz1cInRleHQtd2FybmluZ1wiPiR7ZGV0YWlsLnJldmlld3NQbGFjZTNTdGFyc308L3NwYW4+XG4gICAgICAgICAgJHtkZXRhaWwucmV2aWV3c1BsYWNlM1JhdGluZ30gc3RhcnNcbiAgICAgICAgICA8cD4ke2RldGFpbC5yZXZpZXdzUGxhY2UzVGV4dH08L3A+XG4gICAgICAgICAgPHNtYWxsIGNsYXNzPVwidGV4dC1tdXRlZFwiPiR7ZGV0YWlsLnJldmlld3NQbGFjZTNBdXRob3J9ICR7ZGV0YWlsLnJldmlld3NQbGFjZTFUaW1lfTwvc21hbGw+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG5cbiAgICA8L2Rpdj5cbiAgICBgXG4gICk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBsYWNlRGV0YWlsc1ZpZXc7XG4iLCJpbXBvcnQgaHRtbFRvRWxlbWVudCBmcm9tIFwiLi4vdXRpbHMvaHRtbFRvRWxlbWVudFwiO1xuaW1wb3J0IGRlbGVnYXRlRXZlbnQgZnJvbSBcIi4uL3V0aWxzL2RlbGVnYXRlRXZlbnRcIjtcblxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoXCJldmVudHNcIik7XG5cbi8qKlxuICogR2VzdGlvbmEgbGEgdmlzdGEgZGVsIGdyaWQgZGUgbG9zIGx1Z2FyZXMuXG4gKi9cbmNsYXNzIFBsYWNlc0dyaWRWaWV3IGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gb3B0aW9ucy5lbGVtZW50XG4gICAqL1xuXG4gIGNvbnN0cnVjdG9yKHsgZWxlbWVudCB9KSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudDtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmljaWEgZWwgY29tcG9uZW50ZVxuICAgKi9cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5fc2V0dXBQbGFjZUlkU2VsZWN0aW9uKCk7XG4gIH1cblxuICAvKipcbiAgICogSW5pY2lhIGVsIGV2ZW50byBwYXJhIHNlbGVjY2lvbmFyIHVuIGx1Z2FyIGRlIGxvcyBxdWUgc2UgZW5jdWVudHJhbiBlbiBlbCBncmlkIGRlIGx1Z2FyZXNcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9zZXR1cFBsYWNlSWRTZWxlY3Rpb24oKSB7XG4gICAgZGVsZWdhdGVFdmVudCh0aGlzLl9lbGVtZW50LCBcImNsaWNrXCIsIFwiW3BsYWNlLWlkXVwiLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IGlkID0gZXZlbnQuZGVsZWdhdGVUYXJnZXQuZ2V0QXR0cmlidXRlKFwicGxhY2UtaWRcIik7XG4gICAgICBjb25zb2xlLmxvZyhpZCk7XG4gICAgICB0aGlzLmVtaXQoXCJwbGFjZUlkU2VsZWN0ZWRcIiwgeyBpZCB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNdWVzdHJhIGxvcyBsdWdhcmVzIGVuIGVsIGdyaWQgZGUgbHVnYXJlc1xuICAgKiBAcGFyYW0gIHtPYmplY3R9IHBsYWNlc1xuICAgKi9cbiAgc2hvdyhwbGFjZXMpIHtcbiAgICB0aGlzLl9lbGVtZW50LmlubmVySFRNTCA9IFwiXCI7XG4gICAgcGxhY2VzLmZvckVhY2goKHBsYWNlKSA9PlxuICAgICAgdGhpcy5fZWxlbWVudC5hcHBlbmRDaGlsZChyZW5kZXJQbGFjZXNHcmlkKHBsYWNlKSlcbiAgICApO1xuICB9XG59XG5cbi8qKlxuICogRGV2dWVsdmUgdW4gZWxlbWVudG8gSFRNTCBxdWUgcmVwcmVzZW50YSBsb3MgbHVnYXJlcyBxdWUgY29ycmVzcG9uZW4gYSBsYSBjYXRlZ29yw61hIHNlbGVjY2lvbmFkYVxuICogQHBhcmFtICB7T2JqZWN0fSBwbGFjZVxuICogQHJldHVybiB7RWxlbWVudH1cbiAqL1xuZnVuY3Rpb24gcmVuZGVyUGxhY2VzR3JpZChwbGFjZSkge1xuICByZXR1cm4gaHRtbFRvRWxlbWVudChcbiAgICBgIDxkaXYgY2xhc3M9XCJjb2wtbGctNCBjb2wtbWQtNiBtYi00XCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkIGgtMTAwXCI+XG4gICAgICAgICAgPGEgaHJlZj1cIiNcIj48aW1nIGNsYXNzPVwiY2FyZC1pbWctdG9wXCIgc3JjPVwiJHtwbGFjZS5waG90b3N9XCIgYWx0PVwiXCI+PC9hPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cbiAgICAgICAgICAgIDxoNCBjbGFzcz1cImNhcmQtdGl0bGVcIj5cbiAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIiBwbGFjZS1pZD1cIiR7cGxhY2UuaWR9XCI+JHtwbGFjZS5uYW1lfTwvYT5cbiAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgICA8cD48c3Ryb25nPiR7cGxhY2UudmljaW5pdHl9PC9zdHJvbmc+PC9wPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWZvb3RlclwiPlxuICAgICAgICAgICAgPHNtYWxsIGNsYXNzPVwidGV4dC13YXJuaW5nXCI+JHtwbGFjZS5yYXRpbmd9PC9zbWFsbD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5gXG4gICk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBsYWNlc0dyaWRWaWV3O1xuIiwidmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoXCJldmVudHNcIik7XG5cbi8qKlxuICogR2VzdGlvbmEgbGEgdmlzdGEgZGVsIGxhIGJhcnJhIGRlIGLDunNxdWVkYSBkZSBsdWdhcmVzLlxuICovXG5jbGFzcyBTZWFyY2hCYXJWaWV3IGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gb3B0aW9ucy5lbGVtZW50XG4gICAqL1xuXG4gIGNvbnN0cnVjdG9yKHsgZWxlbWVudCB9KSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudDtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmljaWEgZWwgY29tcG9uZW50ZVxuICAgKi9cbiAgc3RhcnQoKSB7XG4gICAgdGhpcy5fc2V0dXBTZWxlY3RTZWFyY2hCYXJJbnB1dCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEluaWNpYSBlbCBldmVudG8gcGFyYSBlbWl0aXIgZWwgaW5wdXQgcXVlIGludHJvZHVjZSBlbCB1c3VhcmlvIGVuIGxhIGJhcnJhIGRlIGLDunNxdWVkYVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX3NldHVwU2VsZWN0U2VhcmNoQmFySW5wdXQoKSB7XG4gICAgdGhpcy5fZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwic3VibWl0XCIsIChlKSA9PiB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBjb25zdCBzZWFyY2ggPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlYXJjaEJhcklucHV0XCIpLnZhbHVlO1xuICAgICAgY29uc29sZS5sb2coc2VhcmNoKVxuICAgICAgdGhpcy5lbWl0KFwiaW5wdXRcIiwgeyBzZWFyY2ggfSk7XG4gICAgfSk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgU2VhcmNoQmFyVmlldztcbiIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiaW1wb3J0IENhdGVnb3JpZXNWaWV3IGZyb20gXCIuL3ZpZXdzL0NhdGVnb3JpZXNWaWV3XCI7XG5pbXBvcnQgUGxhY2VzR3JpZFZpZXcgZnJvbSBcIi4vdmlld3MvUGxhY2VzR3JpZFZpZXdcIjtcbmltcG9ydCBTZWFyY2hCYXJWaWV3IGZyb20gXCIuL3ZpZXdzL1NlYXJjaEJhclZpZXdcIjtcbmltcG9ydCBQbGFjZXNUcmFja2VyQ29udHJvbGxlciBmcm9tIFwiLi9jb250cm9sbGVycy9QbGFjZXNUcmFja2VyQ29udHJvbGxlclwiO1xuaW1wb3J0IFBsYWNlc1RyYWNrZXJNb2RlbCBmcm9tIFwiLi9tb2RlbHMvUGxhY2VzVHJhY2tlck1vZGVsXCI7XG5pbXBvcnQgUGxhY2VEZXRhaWxzVmlldyBmcm9tIFwiLi92aWV3cy9QbGFjZURldGFpbHNWaWV3XCI7XG5cbmNvbnN0IHNlbGVjdENhdGVnb3JpZXNWaWV3RWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2F0ZWdvcnlMaXN0XCIpO1xuY29uc3QgY2F0ZWdvcmllc1ZpZXcgPSBuZXcgQ2F0ZWdvcmllc1ZpZXcoeyBlbGVtZW50OiBzZWxlY3RDYXRlZ29yaWVzVmlld0VsZW1lbnQgfSk7XG5cbmNvbnN0IHBsYWNlc0dyaWRWaWV3RWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicGxhY2VzXCIpO1xuY29uc3QgcGxhY2VzR3JpZFZpZXcgPSBuZXcgUGxhY2VzR3JpZFZpZXcoeyBlbGVtZW50OiBwbGFjZXNHcmlkVmlld0VsZW1lbnQgfSk7XG5cbmNvbnN0IHBsYWNlRGV0YWlsc1ZpZXdFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkZXRhaWxzXCIpO1xuY29uc3QgcGxhY2VEZXRhaWxzVmlldyA9IG5ldyBQbGFjZURldGFpbHNWaWV3KHsgZWxlbWVudDogcGxhY2VEZXRhaWxzVmlld0VsZW1lbnQgfSk7XG5cbmNvbnN0IHNlYXJjaEJhclZpZXdFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzZWFyY2hCYXJGb3JtXCIpO1xuY29uc3Qgc2VhcmNoQmFyVmlldyA9IG5ldyBTZWFyY2hCYXJWaWV3KHsgZWxlbWVudDogc2VhcmNoQmFyVmlld0VsZW1lbnQgfSk7XG5cbmNvbnN0IHBsYWNlc1RyYWNrZXJNb2RlbCA9IG5ldyBQbGFjZXNUcmFja2VyTW9kZWwocGxhY2VzKTtcblxuY29uc3QgcGxhY2VzVHJhY2tlckNvbnRyb2xsZXIgPSBuZXcgUGxhY2VzVHJhY2tlckNvbnRyb2xsZXIoe1xuICBjYXRlZ29yaWVzVmlldyxcbiAgcGxhY2VzR3JpZFZpZXcsXG4gIHBsYWNlc1RyYWNrZXJNb2RlbCxcbiAgc2VhcmNoQmFyVmlldyxcbiAgcGxhY2VEZXRhaWxzVmlldyxcbn0pO1xuXG5wbGFjZXNUcmFja2VyQ29udHJvbGxlci5zdGFydCgpO1xuXG53aW5kb3cucGxhY2VzVHJhY2tlckNvbnRyb2xsZXIgPSBwbGFjZXNUcmFja2VyQ29udHJvbGxlcjtcbiJdLCJzb3VyY2VSb290IjoiIn0=